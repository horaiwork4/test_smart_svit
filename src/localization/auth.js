export const AUTH = {
  DESCRIPTION_FIRST: {
    RU: "Платформа для бизнеса.",
    EN: "Platform for business.",
    UA: "Платформа для бізнесу",
  },
  DESCRIPTION_SECOND: {
    RU: "Живи смарт",
    EN: "Live smart",
    UA: "Живи смарт",
  },
  FORM_HEADLINER: {
    RU: "Добро пожаловать",
    EN: "Welcome",
    UA: "Ласкаво просимо",
  },
  PHONE_LABEL: {
    RU: "Введите телефон",
    EN: "Enter phone number",
    UA: "Введіть телефон",
  },
  PASS_LABEL: {
    RU: "Введите пароль",
    EN: "Enter password",
    UA: "Введіть пароль",
  },
  BUTTON: {
    RU: "Войти",
    EN: "Log in",
    UA: "Увійти",
  },
  FORGOT: {
    RU: "Забыли пароль?",
    EN: "Restore password",
    UA: "Забули пароль?",
  },
  CHOOSE_COMPANY: {
    RU: "Выбор компании",
    EN: "Choose a company",
    UA: "Вибір компанії",
  },
};
