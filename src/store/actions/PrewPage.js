import { setIsActivePrewPage } from "../actions/actions";

export function changeIsActivePage(boolean) {
  return async (dispatch) => {
    await dispatch(setIsActivePrewPage(boolean));
  };
}