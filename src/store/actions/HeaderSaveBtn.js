import { setHeaderSaveBtn } from "../actions/actions";

export function setHeaderSaveButton(func) {
  return async (dispatch) => {
    await dispatch(setHeaderSaveBtn(func));
  };
}