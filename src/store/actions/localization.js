import { setLanguages, setLanguage } from "./actions";

export function changeLanguage(languages, language) {
  localStorage.setItem("lang", language);

  return async (dispatch) => {
    const newLanguages = languages.map((lang) => {
      const newLang = { ...lang };

      newLang.active = newLang.value === language;

      return newLang;
    });

    await dispatch(setLanguages(newLanguages));
    await dispatch(setLanguage(language));
  };
}