import { LANGUAGES, LANGUAGE, ISACTIVEPREWPAGE, HEADERSAVEBTN } from "./actionTypes";

export function setLanguages(lang) {
  return {
    type: LANGUAGES,
    payload: lang
  };
}

export function setLanguage(lang) {
  return {
    type: LANGUAGE,
    payload: lang
  };
}

export function setIsActivePrewPage(boolean) {
  return {
    type: ISACTIVEPREWPAGE,
    payload: boolean
  }
}

export function setHeaderSaveBtn(func) {
  return {
    type: HEADERSAVEBTN,
    payload: func
  }
}
