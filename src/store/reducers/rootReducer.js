import { combineReducers } from "redux";

import language from "./languageReducer";
import prewPage from "./PrewPageReducer";
import saveFunction from "./headerSaveReducer";

export default combineReducers({
  language: language,
  prewPage: prewPage,
  saveFunction: saveFunction,
});
