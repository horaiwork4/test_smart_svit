import { ISACTIVEPREWPAGE } from "../actions/actionTypes";

const initialState = {
  prewPage: false,
};

export default function prewPage(state = initialState, action) {
  switch (action.type) {
    case ISACTIVEPREWPAGE:
      return {
        ...state,
        prewPage: action.payload,
      };
    default:
      return state;
  }
}
