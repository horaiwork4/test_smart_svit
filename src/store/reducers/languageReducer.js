import { LANGUAGES, LANGUAGE } from "../actions/actionTypes";

const initialState = {
  languages: [
    { value: "EN", active: false },
    { value: "RU", active: false },
    { value: "UA", active: false }
  ],
  language: "RU"
};

export default function language(state = initialState, action) {
  switch (action.type) {
    case LANGUAGES:
      return {
        ...state,
        languages: action.payload
      };
    case LANGUAGE:
      return {
        ...state,
        language: action.payload
      };

    default:
      return state;
  }
}
