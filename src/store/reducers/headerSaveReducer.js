import { HEADERSAVEBTN } from "../actions/actionTypes";

const initialState = {
  saveFunction: false,
};

export default function saveFunction(state = initialState, action) {
  switch (action.type) {
    case HEADERSAVEBTN:
      return {
        ...state,
        saveFunction: action.payload,
      };
    default:
      return state;
  }
}