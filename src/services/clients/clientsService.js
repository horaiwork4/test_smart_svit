export class ClientsService {
  constructor(link) {
    this.link = link;
  }

  getClientCategories = async (companyId) => {
    const url = `${this.link}api/company/${companyId}/clien-category`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  createNewClientsCategory = (company_id, catName) => {
    const url = `${this.link}api/company/${company_id}/add-new-clien-category`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = {
      name: catName,
    };

    // formData.append("name", catName);

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(formData),
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  getClientsInCategory = async (companyId, category, page) => {
    const url = `${this.link}api/company/${companyId}/clien-category/${category}?page=${page}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  getClientsList = async (companyId) => {
    const url = `${this.link}api/company/${companyId}/client-list`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  addCategory = async (companyId, catName) => {
    const url = `${this.link}api/company/${companyId}/add-new-clien-category`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = {
      name: catName,
    };

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: JSON.stringify(formData),
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  editCategory = async (companyId, categodyId, categoryName) => {
    const url = `${this.link}api/company/${companyId}/clien-category`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = {};

    formData.name = categoryName;
    formData.category_id = categodyId;
    formData.company_id = companyId;

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-type", "application/json");

    const requestOptions = {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(formData),
      redirect: "follow",
    };

    return fetch(url, requestOptions).then((data) => data.json());
  };

  companyInviteUser = async (companyId, phone) => {
    const url = `${this.link}api/company/${companyId}/invit-user`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();

    formData.append("phone", phone);

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    return fetch(url, requestOptions)
      .then((data) => data.text())
      .catch((error) => console.log(error));
  };

  clientSearch = async (phone) => {
    const url = `${this.link}api/profile/user-search`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();

    formData.append("search", phone);

    console.log(phone);

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    return fetch(url, requestOptions)
      .then((data) => data.text())
      .catch((error) => console.log(error));
  };
}
