export class SalesService {
  constructor(link) {
    this.link = link;
  }

  getSales = async (company_id, page) => {
    const url = `${this.link}api/company/${company_id}/promotions?page=${page}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-type", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
    };

    let response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };

  newPromotion = async (
    compID,
    name,
    desc,
    startDateSale,
    endDateSale,
    images,
    nameLink,
    link,
    nameCoupon,
    descCoupon,
    discont
  ) => {
    const url = `${this.link}api/create-promotion`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();

    headers.append("Authorization", "Bearer " + token);

    formData.append("company_id", compID);
    formData.append("name", name);
    formData.append("description", desc);
    formData.append("startDateTime", startDateSale);
    formData.append("endDateTime", endDateSale);
    formData.append("image", images);

    console.log(images);

    formData.append("link", link);
    formData.append("coupon_name", nameCoupon);
    formData.append("coupon_description", descCoupon);
    formData.append("discount", discont);

    const requestOptions = {
      method: "POST",
      body: formData,
      headers: headers,
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  deleteProm = async (company_id, prom_id) => {
    const url = `${this.link}api/company/${company_id}/promotion/${prom_id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "DELETE",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };
}
