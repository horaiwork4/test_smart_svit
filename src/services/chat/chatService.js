export class ChatService {
  constructor(link) {
    this.link = link;
  }

  getCompanyChats = async (companyId) => {
    const url = `${this.link}api/company/${companyId}/chats`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
  
    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");
  
    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };
  
    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);
  
    return data;
  }
  
   getChatWithId = async (userFrom, chatId) => {
    const url = `${this.link}api/chats/${chatId}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();
  
    formData.append("company_id", userFrom);
  
    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");
  
    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };
  
    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);
  
    return data;
  }
  
   sendMessage = async (userFrom, chatId, messageBody, files) => {
    const url = `${this.link}api/chats/${chatId}/message`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();
  
    formData.append("chat_id", chatId);
    formData.append("from", "company_id: " + userFrom);
    formData.append("body", messageBody);
    files.length > 0 && formData.append("file", files);
  
    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");
  
    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };
  
    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);
  
    return data;
  }
  
   uploadFiles = async (chatId, file) => {
    const url = `${this.link}api/chats/${chatId}/upload-file`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
  
    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "multipart/form-data");
  
    const formdata = new FormData();
    formdata.append("file", file, file.name);
  
    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formdata,
      redirect: "follow",
    };
  
    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);
  
    return data;
  }
}