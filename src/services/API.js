import { AdvServise } from "./advertisment/advService";
import { AuthService } from "./auth/authService";
import { ChatService } from "./chat/chatService";
import { CompanyService } from "./company/companyService";
import { DeliveryService } from "./delivery/deliveryService";
import { SalesService } from "./sales/salesService";
import { FoodService } from "./food/foodService";
import { ClientsService } from "./clients/clientsService";

let api = null;

export class API {
  constructor(isDev) {
    this.prodLink = "https://smartsvit.chost.com.ua/";
    this.devLink = "http://smartsvit.shn-host.ru/";
    const link = isDev ?  this.devLink : this.prodLink;

    this.advService = new AdvServise(link);
    this.authService = new AuthService(link);
    this.chatService = new ChatService(link);
    this.companyService = new CompanyService(link);
    this.deliveryService = new DeliveryService(link);
    this.foodService = new FoodService(link);
    this.salesSerivce = new SalesService(link);
    this.clientsService = new ClientsService(link);
  }
  getAdv = () => {
    return this.advService;
  };
  getAuth = () => {
    return this.authService;
  };
  getChat = () => {
    return this.chatService;
  };
  getCompany = () => {
    return this.companyService;
  };
  getDelivery = () => {
    return this.deliveryService;
  };
  getFood = () => {
    return this.foodService;
  };
  getSales = () => {
    return this.salesSerivce;
  };
  getClients = () => {
    return this.clientsService;
  };
}

export const getApi = () => {
  if (!api) api = new API(false);
  return api;
};
