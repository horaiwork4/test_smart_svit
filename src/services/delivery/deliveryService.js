export class DeliveryService {
  constructor(link) {
    this.link = link;
  }

  getAttributes = async (companyId) => {
    const url = `${this.link}api/company/${companyId}/food-delivery/attributes`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  getDelivery = async (companyId) => {
    const url = `${this.link}api/company/${companyId}/food-delivery`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  createAdditionalCategory = async (companyId, name) => {
    const url = `${this.link}api/company/${companyId}/food-delivery/add-attribute-group`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("name", name);
    formData.append("type", 1);

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  createAdditionalProduct = async (companyId, catId, name, cost) => {
    const url = `${this.link}api/company/${companyId}/food-delivery/add-attribute`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("name", name);
    formData.append("attribute_group_id", catId);
    formData.append("price", cost);

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  updateAttributeGroup = async (company_id, group_id, name, type) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/attribute-group/${group_id}`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("name", name);
    // formData.append("attribute_group_id", catId);
    // formData.append("price", cost);

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  showFoodCategory = async () => {
    const url = `${this.link}api/food-delivery/category`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => console.log(error));
  };

  showFoodCategoryCompany = async (id) => {
    const url = `${this.link}api/company/${id}/food-delivery/categorys`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  //  fDeleteAttribute = async (company_id, attribute_id) => {
  //   const url = `${this.link}api/company/${company_id}/food-delivery/attribute/${attribute_id}`;
  //   const headers = new Headers();
  //   const token = localStorage.getItem("token");
  //   return await fetch(url, requestOptions)
  //     .then((data) => data.json())
  //     .catch((error) => console.log(error));
  // };

  createFoodCategory = async (catName, company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/add-category`;
    const token = localStorage.getItem("token");
    const formdata = new FormData();
    const headers = new Headers();

    formdata.append("name", catName);

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "DELETE",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  updateAttribute = async (
    company_id,
    attribute_id,
    attribute_group_id,
    name,
    cost
  ) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/attribute/${attribute_id}`;
    const headers = new Headers();
    const token = localStorage.getItem("token");

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("attribute_group_id", attribute_group_id);
    formData.append("name", name);
    formData.append("price", cost);

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data;
  };

  updateCompany = async (desc, delivery, category, company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/update`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    var formdata = new FormData();
    formdata.append("description", desc);
    formdata.append("delivery_price", delivery);
    formdata.append("category", JSON.stringify(category));

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formdata,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.text())
      .catch((error) => console.log(error));
  };

  showDelivery = async (company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/x-www-form-urlencoded");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.text())
      .catch((error) => console.log(error));
  };
}
