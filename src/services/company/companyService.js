export class CompanyService {
  constructor(link) {
    this.link = link;
  }

  getComapny = async (token) => {
    const url = `${this.link}api/profile`;
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();
    return data.company != null ? data.company : "error";
  };

  getNews = async (company_id) => {
    const url = `${this.link}api/company/${company_id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);

    return data;
  };
}
