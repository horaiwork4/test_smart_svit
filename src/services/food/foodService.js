import axios from "axios";

export class FoodService {
  constructor(link) {
    this.link = link;
  }

  getCompanysCategories = async (company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/categorys`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  getCompanysProductFromCatId = async (company_id, cat_id, page) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/categorys/${cat_id}?page=${page}`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  getCompanyOrders = async (company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/orders`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  changeOrderStatus = async (company_id, order_status, id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/orders/${id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formdata = new FormData();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    formdata.append("order_status", order_status);

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formdata,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };
  getCompanyAttributes = async (company_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/attributes`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  addFoodCategory = async (company_id, name) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/add-category`;

    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();
    formData.append("name", name);

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  addProduct = async (
    food_delivery_category_id,
    name,
    description,
    vendor_code,
    price,
    image,
    attribute,
    option,
    related,
    company_id
  ) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/add-product`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("food_delivery_category_id", +food_delivery_category_id);
    formData.append("name", name);
    formData.append("description", description);
    formData.append("vendor_code", vendor_code);
    formData.append("price", price);
    formData.append("image", JSON.stringify(image));
    formData.append("attribute", JSON.stringify(attribute));
    formData.append("option", JSON.stringify(option));
    formData.append("related", JSON.stringify(related));

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  uploadPhoto = async (inputRef) => {
    const url = `${this.link}api/upload`;

    const token = localStorage.getItem("token");

    let formData = new FormData();
    formData.append("image", inputRef, inputRef.name);

    return await axios.post(url, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Bearer " + token,
      },
    });
  };

  updateProduct = async (
    food_delivery_category_id,
    name,
    description,
    vendor_code,
    price,
    image,
    attribute,
    option,
    related,
    company_id,
    product_id
  ) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/product/${product_id}`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("food_delivery_category_id", +food_delivery_category_id);
    formData.append("name", name);
    formData.append("description", description);
    formData.append("vendor_code", vendor_code);
    formData.append("price", price);
    formData.append("image", JSON.stringify(image));
    formData.append("attribute", JSON.stringify(attribute));
    formData.append("option", JSON.stringify(option));
    formData.append("related", JSON.stringify(related));

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  getProduct = async (company_id, product_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/product/${product_id}`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  deleteProduct = async (company_id, product_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/product/${product_id}`;

    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "DELETE",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => alert(error));
  };

  deleteFoodCat = async (company_id, cat_id) => {
    const url = `${this.link}api/company/${company_id}/food-delivery/category/${cat_id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "DELETE",
      headers: headers,
      redirect: "follow",
    };

    return await fetch(url, requestOptions)
      .then((data) => data.json())
      .catch((error) => console.log(error));
  };
}
