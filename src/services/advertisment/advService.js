import axios from "axios";

export class AdvServise {
  constructor(link) {
    this.link = link;
  }

  sectorsListService = async () => {
    const url = `${this.link}api/sectors`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "GET",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };

  getListAdvertisment = async (company_id) => {
    const url = `${this.link}api/company/${company_id}/boards-list`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };

  deleteAdv = async (company_id, board_id) => {
    const url = `${this.link}api/company/${company_id}/board/${board_id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const requestOptions = {
      method: "DELETE",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };

  createAdFirstStep = async (id) => {
    const url = `${this.link}api/board/create`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = new FormData();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    formData.append("company_id", id);

    const requestOptions = {
      method: "POST",
      headers: headers,
      body: formData,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };

  uploadPhoto = async (inputRef) => {
    const url = `${this.link}api/upload`;

    const token = localStorage.getItem("token");

    let formData = new FormData();
    formData.append("image", inputRef, inputRef.name);

    console.log(formData);

    return await axios.post(url, formData, {
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: "Bearer " + token,
      },
    });
  };

  createAdSecondStep = async (id, images, board_type, houses, name, desc) => {
    const url = `${this.link}api/board/${id}`;
    const token = localStorage.getItem("token");
    const headers = new Headers();
    const formData = {};

    headers.append("Authorization", "Bearer " + token);
    headers.append("Content-Type", "application/json");

    formData.images = images;
    formData.name = name;
    formData.options = [];
    formData.house = houses;
    formData.type = board_type;
    formData.description = desc;

    const requestOptions = {
      method: "PUT",
      headers: headers,
      body: JSON.stringify(formData),
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json().catch((error) => error);
    return data;
  };

  createAd = async (
    company_id,
    name,
    desc,
    type,
    startDateTime,
    endDateTime,
    image,
    link,
    link_name,
    coupon_name,
    coupon_desc,
    discount,
    coupon_color,
    text_color,
    images
  ) => {
    const url = `http://smartsvit.shn-host.ru/api/create-promotion`;
    const token = localStorage.getItem("token");
    const headers = new Headers();

    headers.append("Authorization", "Bearer " + token);
    headers.append("Accept", "application/json");

    const formData = new FormData();
    formData.append("company_id", company_id);
    formData.append("name", name);
    formData.append("desc", desc);
    formData.append("type", type);
    formData.append("startDateTime", startDateTime);
    formData.append("endDateTime", endDateTime);
    formData.append("image", image);
    formData.append("link", link);
    formData.append("link_name", link_name);
    formData.append("coupon_name", coupon_name);
    formData.append("coupon_desc", coupon_desc);
    formData.append("discount", discount);
    formData.append("coupon_color", coupon_color);
    formData.append("text_color", text_color);
    formData.append("discount", discount);
    formData.append("images", images);

    const requestOptions = {
      method: "POST",
      headers: headers,
      redirect: "follow",
    };

    const response = await fetch(url, requestOptions);
    const data = await response.json();

    return data;
  };
}
