import React from "react";
import spinner from "../../../../public/img/spinner.svg";
import "../../sass/Loader.scss";

export function Loader() {
  return (
    <div className={"LoaderWrapper"}>
      <img src={spinner} alt="spinner" className={"LoaderWrapper__spinner"} />
    </div>
  );
}
