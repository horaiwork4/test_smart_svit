import React from "react";
import "../../sass/Modal.scss";
import { Button } from "./Button";

export function Modal({
  handleClose,
  show,
  children,
  title,
  onAccept,
  isRangePicker,
  valid,
  closeLabel,
  acceptLabel,
  hideAccept = false,
}) {
  const showHideClassName = show ? "Modal display-block" : "Modal display-none";
  const mainClassName = isRangePicker ? "Modal__mainPicker" : "Modal__main";

  return (
    <div className={showHideClassName}>
      <section className={mainClassName}>
        <div className="Modal__title">{title}</div>
        <div className="Modal__children">{children}</div>
        <div className="Modal__actions">
          <Button
            onCLick={handleClose}
            label={closeLabel ? closeLabel : "Назад"}
            className="Modal__hide-btn fullButton"
          />
          {
            !hideAccept &&
            <Button
              disabled={!!valid}
              onCLick={onAccept}
              label={acceptLabel ? acceptLabel : "Применить"}
              className="Modal__aprove-btn"
            />
          }
        </div>
      </section>
    </div>
  );
}
