import React from "react";

export const Input = ({
  className,
  placeholder,
  label,
  type = "text",
  value,
  onChange,
  srcIcon
}) => {
  const inputClassName = ["Input__field", className].join(" ");
  const htmlFor = `${type}-${Math.random()}`;

  const simpleInput = (
    <div className={"Input"}>
      <label htmlFor={htmlFor} className={"Input__label"}>
        {label}
      </label>

      <input
        id={htmlFor}
        className={inputClassName}
        placeholder={placeholder}
        value={value}
        type={type}
        onChange={onChange}
      />
    </div>
  );

  const iconInput = (
    <div className={"InputIcon"}>
      <label htmlFor={htmlFor} className={"InputIcon__label"}>
        {label}
      </label>

      <div className={"InputIcon__container"}>
        <img
          srcSet={srcIcon}
          className={"InputIcon__icon"}
          alt={"Input Icon"}
        />
        <input
          id={htmlFor}
          className={"InputIcon__field"}
          placeholder={placeholder}
          value={value}
          type={type}
          onChange={onChange}
        />
      </div>
    </div>
  );

  return <>{srcIcon ? iconInput : simpleInput}</>;
};
