import React from "react";
import "../../sass/ActionButton.scss";

export function ActionButton({ icon, title, onClick }) {
  return (
    <div className={"ActionButton"}>
      <img className={"ActionButton__img"} src={icon} alt="icon" />
      <button className={"ActionButton__button"} onClick={onClick}>
        {title}
      </button>
    </div>
  );
}
