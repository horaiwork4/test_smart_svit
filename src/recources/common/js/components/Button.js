import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";

export const Button = ({
  type,
  label,
  className,
  disabled,
  loading = false,
  onCLick,
}) => {
  const buttonClassName = ["Button", className].join(" ");

  return (
    <button
      type={type}
      disabled={disabled}
      className={
        // eslint-disable-next-line
        disabled ? buttonClassName + " " + "Button_disabled" : buttonClassName
      }
      onClick={onCLick}
    >
      {loading ? (
        <FontAwesomeIcon
          icon={faSpinner}
          className={"Button_loopLoading"}
          size="lg"
        />
      ) : (
        label
      )}
    </button>
  );
};
