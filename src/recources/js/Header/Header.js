import React from "react";

import "./styles/Header.scss";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

export const Header = ({ company }) => {
  const history = useHistory();

  const isActiveBackPage = useSelector((state) => state.prewPage.prewPage);
  const saveFunction = useSelector((state) => state.saveFunction.saveFunction);

  return (
    <div className={"Header"}>
      <div className={"Header_searchBox"}>
        <div className={"Header_searchBox_name"}>
          <p className={"Header_name pointer"} onClick={isActiveBackPage && history.goBack}>
            {isActiveBackPage ? "Назад" : "Главная"}
          </p>

          <p className={"Header_name pointer"} onClick={saveFunction}>
            {saveFunction && "Сохранить"}
          </p>
        </div>
      </div>

      <div className={"Header_companyInfo"}>
        {/*Димон видимо не перенёс аватары на дев сервер*/}
        <img
          src={company && "http://smartsvit.shn-host.ru" + company.avatar}
          className={"Header_companyInfo_image"}
          alt={"company_info"}
        />
        <p className={"Header_companyInfo_name"}>{company && company.name}</p>
        <img
          src={require("../../../public/img/dropDownMenu.png")}
          className={"Header_companyInfo_dropMenu"}
          alt={"dropmenu"}
        />
      </div>
    </div>
  );
};
