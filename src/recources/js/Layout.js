import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { Auth } from "./Auth/Auth";
import { MainPage } from "./MainPage/MainPage";

export const Layout = () => {
  return (
    <Router>
      <Switch>
        <Route strict path="/login">
          <Auth />
        </Route>
        <Route strict path="/">
          <MainPage />
        </Route>
      </Switch>
    </Router>
  );
};
