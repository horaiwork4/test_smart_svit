import React, { useState, useEffect } from "react";
import { Input } from "../../Components/Input/Input";
import { Button } from "../../../common/js/components/Button";
import { Modal } from "../../../common/js/components/Modal";
import "./style/styles.scss";
import { useHistory } from "react-router-dom";

import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { setHeaderSaveBtn } from "../../../../store/actions/actions";
import { getApi } from "../../../../services/API";

export function DeliveryInfo({ company }) {
  const api = getApi().getDelivery();
  // const [downloaded, setDownLoaded] = useState(false);

  const [desc, setDesc] = useState("");
  const [descError, setDescError] = useState(false);

  const [price, setPrice] = useState("");
  const [priceError, setPriceError] = useState(false);

  const [allCategorys, setAllCategory] = useState(null);
  const [companyCategories, setCompanyCategories] = useState([]);
  const [catError, setCatError] = useState(false);

  const [visibleModal, setVisibleModal] = useState(false);

  const [compID, setCompID] = useState(null);

  const [dataCompany, setDataCompany] = useState(null);

  const history = useHistory();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeIsActivePage(true));
    dispatch(setHeaderSaveBtn(handleUpdateDelivery));
  }, [dispatch]);

  useEffect(() => {
    if (!!company && !!company.id) {
      setCompID(company.id);
    }
  }, [company]);

  useEffect(() => {
    refresh();
  }, [company]);

  const refresh = () => {
    if (!!company && !!company.id) {
      api.showDelivery(company.id).then((data) => {
        if (!!data && !!JSON.parse(data).shop) {
          setDataCompany(JSON.parse(data));
          setDesc(JSON.parse(data).shop.description);
          setPrice(JSON.parse(data).shop.delivery_price);

          setCompanyCategories(JSON.parse(data).shop.in_categorys);
        }
      });
    }
  };

  useEffect(() => {
    if (!!compID) {
      dispatch(
        setHeaderSaveBtn(
          handleUpdateDelivery.bind(
            null,
            desc,
            price,
            companyCategories.map((item) => item.id),
            compID
            // companyCategories
          )
        )
      );
    }
  }, [desc, price, companyCategories, compID]);

  useEffect(() => {
    api.showFoodCategory().then((data) => {
      data.map((item, index) => {
        const newItem = { ...item };

        newItem.tapped = false;

        return newItem;
      });

      setAllCategory([...data]);
    });
  }, []);

  const compCat =
    !!companyCategories &&
    companyCategories.length > 0 &&
    companyCategories.map((item) => {
      if (!!item && !!item.name) {
        return (
          <div key={item.id} className={"DeliveryInfo__catWrapper-item"}>
            {item.name}
          </div>
        );
      }
    });

  const showModal = () => {
    setVisibleModal(true);
  };

  const hideModal = () => {
    setVisibleModal(false);
  };

  const handleCatClick = (index) => {
    allCategorys[index].tapped = !allCategorys[index].tapped;
    const newMyCats = allCategorys.filter((item) => item.tapped);

    setAllCategory([...allCategorys]);
    setCompanyCategories(newMyCats);
  };

  const availableCategories =
    allCategorys &&
    allCategorys.map((item, index) => {
      return (
        <button
          key={item.id}
          className={item.tapped ? "DeliveryInfo__allCatBtn-tapped" : "DeliveryInfo__allCatBtn"}
          onClick={handleCatClick.bind(null, index)}
        >
          {item.name}
        </button>
      );
    });

  const checkForm = () => {
    let error = false;

    if (desc.length < 10) {
      setDescError(true);
      error = true;
    }

    if (price.length < 1) {
      setPriceError(true);
      error = true;
    }

    if (companyCategories.length < 1) {
      setCatError(true);
      error = true;
    }

    return error;
  };

  const handleUpdateDelivery = (desc, price, companyCategories, compID) => {
    if (!checkForm() && compID) {
      api.updateCompany(desc, price, companyCategories, compID).then((data) => {
        if (!!data && JSON.parse(data).status === "success") {
          hideModal();
          refresh();
        }
      });
    }
  };

  return (
    <div>
      <Input
        name={"О доставке"}
        defaultValue={"О доставке *"}
        value={desc}
        helperText={descError && `Минимум символов ${desc.length}/10`}
        setValue={setDesc}
        error={descError}
        withEndLine={true}
        type={"text"}
      />
      <Input
        name={"Цена"}
        defaultValue={"Цена *"}
        value={price}
        helperText={priceError && `Минимум символов ${price.length}/10`}
        setValue={setPrice}
        error={priceError}
        withEndLine={true}
        type={"text"}
      />
      <div className={"DeliveryInfo"}>
        <img
          src={require("../../../../public/img/add.png")}
          alt={"add_prod"}
          className={"DeliveryInfo__img"}
        />
        <Button
          className={"DeliveryInfo__add-category"}
          label={"Добавить категорию"}
          onCLick={showModal}
        />
      </div>
      {companyCategories.length <= 0 && (
        <p style={{ color: "red" }}>Выберите миниум 1 категорию!</p>
      )}
      {companyCategories && companyCategories.length > 0 && (
        <div className={"DeliveryInfo__catWrapper"}>{compCat}</div>
      )}
      <Modal
        handleClose={hideModal}
        show={visibleModal}
        children={availableCategories}
        title={"Добавить категорию"}
        closeLabel={"Продолжить"}
        hideAccept={true}
      />
    </div>
  );
}
