import React, { useState, useEffect } from "react";
import "../Delivery/styles/ItemTile.scss";
import "../Delivery/styles/Dishes.scss";
import { Button } from "../../common/js/components/Button";
import { Link } from "react-router-dom";
import { Input } from "../../common/js/components/Input";
import { getApi } from "../../../services/API";

export const ItemTile = ({
  category,
  company,
  handlePress,
  visibleButtons,
  ordersStatus,
  getCompanysCategories,
  pickedId,
}) => {
  const api = getApi().getFood();
  const [name, setName] = useState("");
  const [createNew, setCreateNew] = useState(false);
  const [isSuccesToCreate, setSuccesToCreate] = useState(false);

  useEffect(() => {
    if (name.length >= 3) {
      setSuccesToCreate(true);
    } else {
      setSuccesToCreate(false);
    }
  }, [name]);

  const typeDishTile = category.map((item, index) => {
    return (
      <div key={item.id} className={"DishesTileWrapper"}>
        <Button
          className={
            pickedId === item.id ? "Status DishesTileWrapper_picked" : "Status"
          }
          label={item.name}
          onCLick={
            ordersStatus
              ? () => {
                  handlePress(index);
                }
              : () => {
                  handlePress(item.id);
                }
          }
        />
      </div>
    );
  });

  const createCat = () => {
    api.addFoodCategory(company.id, name).then((data) => {
      if (data.status === "success") {
        getCompanysCategories();
        setName("");
        setSuccesToCreate(false);
        setCreateNew(false);
      }
    });
  };

  return (
    <div className={"ItemTileWrapper"}>
      {visibleButtons ? (
        <Link to={"/delivery/additional"}>
          <Button
            label={"Редактор опций"}
            onCLick={() => {}}
            className={"ItemTileWrapper__edit-btn"}
          />
        </Link>
      ) : null}

      {typeDishTile}

      {createNew ? (
        <div className={"ItemTileWrapper__add-btn-wrapper"}>
          <div style={{ alignSelf: "center", marginBottom: "0.5rem" }}>
            <Input value={name} onChange={(e) => setName(e.target.value)} />
          </div>
          {isSuccesToCreate && (
            <img
              src={require("../../../public/img/add.png")}
              style={{
                marginLeft: "10px",
              }}
              alt={""}
              className={"Additional_add_image"}
              onClick={createCat}
            />
          )}
        </div>
      ) : visibleButtons ? (
        <>
          <div className={"ItemTileWrapper__add-btn-wrapper"}>
            <img
              src={require("../../../public/img/add_category.png")}
              alt={"add_category"}
              className={"ItemTileWrapper__addcategory-btn-image"}
            />
            <Button
              label={"Добавить категорию"}
              onCLick={() => {
                setCreateNew(true);
              }}
              className={"ItemTileWrapper__addcategory-btn"}
            />
          </div>
          <div className={"ItemTileWrapper__info-btn-wrapper"}>
            <img
              src={require("../../../public/img/carbon_information.png")}
              alt={"add_category"}
              className={"ItemTileWrapper__info-btn-image"}
            />
            <Link to={"/delivery/delivery-info"}>
              <Button
                label={"Информация о доставке"}
                className={"ItemTileWrapper__info-btn"}
              />
            </Link>
          </div>
        </>
      ) : null}
    </div>
  );
};
