import React, { useState, useEffect, useRef } from "react";
import { Input } from "../../Components/Input/Input";

import "./style/newProduct.scss";
import { ImageLoader } from "../../Components/ImageLoader/ImageLoader";
import { Modal } from "../../../common/js/components/Modal";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { setHeaderSaveBtn } from "../../../../store/actions/actions";
import { useHistory } from "react-router-dom";
import { getApi } from "../../../../services/API";

export const NewProduct = ({ company, pickedCategoryId, params }) => {
  const api = getApi().getFood();
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState("");

  const [desc, setDesc] = useState("");
  const [descError, setDescError] = useState("");

  const [vendorCode, setVendorCode] = useState("");
  const [productValues, setProductValues] = useState([
    {
      name: "",
      price: "",
    },
  ]);
  const [myOptions, setMyOptions] = useState([]);
  const [photos, setPhotos] = useState([]);
  const [downloadedPhotos, setDownloadedPhotos] = useState([]);
  const [rawPhotos, setRawPhotos] = useState([]);
  const [photosError, setPhotosError] = useState([]);
  const [isVisibleAdditionalModal, setVisibleAdditionalModal] = useState(false);
  const [isCreate, setIsCreate] = useState(true);
  const [downloaded, setDownLoaded] = useState(false);
  const [productId, setProductId] = useState(null);

  const history = useHistory();

  const dispatch = useDispatch();
  const fileRef = useRef();

  useEffect(() => {
    if (company && !downloaded) {
      setDownLoaded(true);
      api.getCompanyAttributes(company.id).then((data) => {
        pickedCategoryId = localStorage.getItem("picked_category");
        setMyOptions(data);

        let pr = JSON.parse(localStorage.getItem("pickedProduct"));
        if (pr) {
          setName(pr.name);
          setDesc(pr.description);
          setProductId(pr.id);
          productValues[0].price = pr.price;
          productValues[0].name = pr.name;
          setVendorCode(pr.vendor_code);
          if (!downloaded) {
            api.getProduct(company ? company.id : 0, pr ? pr.id : 0).then(
              (data2) => {
                data2.option.forEach((item) => {
                  productValues.push(item);
                });
                setProductValues([...productValues]);

                setDownloadedPhotos(data2.images);

                data.forEach((element) =>
                  element.attribute.forEach((item) =>
                    data2.attribute.forEach((pickedItem) => {
                      if (pickedItem.attribute.id == item.id)
                        item.picked = true;
                    })
                  )
                );
                setMyOptions([...data]);
              }
            );
            setIsCreate(false);
          }
        }
      });
    }
  }, [company]);

  useEffect(() => {
    dispatch(changeIsActivePage(true));
    dispatch(setHeaderSaveBtn(saveProduct));
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      setHeaderSaveBtn(
        saveProduct.bind(
          null,
          name,
          desc,
          productValues,
          photos,
          downloadedPhotos,
          productId
        )
      )
    );
  }, [name, desc, productValues, photos, downloadedPhotos, productId]);

  const checkErorrs = (name, desc, productValues, photos) => {
    let error = false;

    //check name
    if (name.length === 0) {
      error = true;
      setNameError(true);
    }

    //check description
    if (desc.length === 0) {
      error = true;
      setDescError(true);
    }

    //check values
    productValues.forEach((item) => {
      if (item.name.length === 0 || item.price === "") {
        error = true;
        item.error = true;
      }
    });

    setProductValues([...productValues]);

    //check image
    if (photos.length === 0) {
      error = true;
      setPhotosError(true);
    }

    return error;
  };

  const saveProduct = (
    name,
    desc,
    productValues,
    photos,
    downloadedPhotos,
    productId
  ) => {
    if (!checkErorrs(name, desc, productValues, photos)) {
      let promises = [];
      rawPhotos.forEach((element) => {
        promises.push(uploadPhotos(element));
      });

      const attributes = [];

      myOptions.forEach((item) =>
        item.attribute.forEach((attribute) => {
          if (attribute.picked) attributes.push(attribute.id);
        })
      );

      var action = isCreate ? api.addProduct : api.updateProduct;

      Promise.all(promises).then((images) => {
        for (let i = downloadedPhotos.length; i > 0; i--) {
          images.unshift(downloadedPhotos[i - 1].url);
        }
        action(
          pickedCategoryId,
          name,
          desc,
          vendorCode,
          productValues[0].price,
          images,
          attributes,
          productValues,
          [],
          company.id,
          productId
        ).then((data) => {
          if (data.status === "success") history.goBack();
        });
      });
    }
  };

  const changeValues = (id, type, value) => {
    if (type === "value") productValues[id].price = value;
    else if (type === "name") productValues[id].name = value;

    setProductValues([...productValues]);
  };

  const addValue = () => {
    productValues.push({
      name: "",
      price: "",
    });

    setProductValues([...productValues]);
  };

  const uploadPhotos = (image) => {
    return new Promise((resolve, reject) => {
      api.uploadPhoto(image).then((data) => {
        resolve(data.data.url);
      });
    });
  };

  const addPhoto = (_photos) => {
    if (_photos.length > 0) {
      rawPhotos.push(_photos[_photos.length - 1]);
      setRawPhotos([...rawPhotos]);

      let path = URL.createObjectURL(_photos[0]);
      photos.push(path);
      setPhotos([...photos]);
    }
  };

  const pickAttribute = (catId, attributeId) => {
    myOptions[catId].attribute[attributeId].picked = !myOptions[catId]
      .attribute[attributeId].picked;

    setMyOptions([...myOptions]);
  };

  const deleteValue = (id) => {
    productValues.splice(id, 1);
    setProductValues([...productValues]);
  };

  const myOptionsBlock = (
    <div className={"AdditionalBlock"}>
      {myOptions.length > 0 &&
        myOptions.map(
          (item, key) =>
            item.attribute.length > 0 && (
              <div className={"AdditionalBlock_additionalPicker"} key={key}>
                <p className={"AdditionalBlock_additionalPicker_catName"}>
                  {item.name}
                </p>
                <div className={"AdditionalBlock_additionalPicker_child"}>
                  {item.attribute.map((item, id) => (
                    <div
                      className={
                        "AdditionalBlock_additionalPicker_child_picker"
                      }
                      onClick={pickAttribute.bind(null, key, id)}
                    >
                      <p className={"AdditionalBlock_additionalPicker_name"}>
                        {item.name}
                      </p>
                      <img
                        className={"AdditionalBlock_additionalPicker_img"}
                        src={
                          item.picked
                            ? require("../../../../public/img/checked.png")
                            : require("../../../../public/img/uncheked.png")
                        }
                      />
                    </div>
                  ))}
                </div>
              </div>
            )
        )}
    </div>
  );

  return (
    <div className={"NewProduct"}>
      <div className={"NewProduct_row"}>
        {downloadedPhotos.map((item, key) => (
          <ImageLoader photo={item.url} key={key} alt={"photo"} isLink={true} />
        ))}

        {photos.map((item, key) => (
          <ImageLoader photo={item} key={key} alt={"photo"} />
        ))}

        <label>
          <ImageLoader error={photosError} />
          <input
            type="file"
            id={"files"}
            ref={fileRef}
            accept=".jpg, .jpeg, .png"
            onChange={(e) => addPhoto(e.currentTarget.files)}
            style={{ display: "none" }}
          />
        </label>
      </div>
      <div className={"NewProduct_row"}>
        <div className={"NewProduct_row_col"}>
          <Input
            name={"Название товара"}
            defaultValue={"Название товара *"}
            value={name}
            setValue={setName}
            error={nameError}
            withEndLine={true}
            type={"text"}
          />
          <Input
            name={"Артикул товара"}
            defaultValue={"AS21Q"}
            value={vendorCode}
            setValue={setVendorCode}
            withEndLine={true}
            type={"text"}
          />
          <Input
            name={"Описание товара"}
            defaultValue={desc.length === 0 ? "Описание товара *" : desc}
            multiLine={true}
            value={desc}
            error={descError}
            setValue={setDesc}
            withEndLine={true}
            type={"text"}
          />
        </div>
        <div className={"NewProduct_row_col"}>
          <p>Цена</p>
          {productValues.map((item, key) => (
            <div key={key}>
              <div className={"ProductNameWithDelete"}>
                <Input
                  name={"Название товара"}
                  defaultValue={"Название товара *"}
                  value={item.name}
                  error={item.error}
                  setValue={changeValues.bind(null, key, "name")}
                  withEndLine={false}
                  type={"text"}
                />
                <img
                  src={require("../../../../public/img/trash.png")}
                  className={"ProductNameWithDelete_trash"}
                  onClick={deleteValue.bind(null, key)}
                />
              </div>

              <Input
                name={"Цена"}
                defaultValue={"Цена *"}
                multiLine={false}
                error={item.error}
                value={item.price}
                setValue={changeValues.bind(null, key, "value")}
                withEndLine={true}
                type={"number"}
              />
            </div>
          ))}
          <div className={"row clickable"} onClick={addValue}>
            <img
              src={require("../../../../public/img/add.png")}
              alt={"add"}
              className={"add"}
            />
            <p className={"text"}>Добавить цену</p>
          </div>
        </div>
        <div className={"NewProduct_row_col"}>
          <Modal
            show={isVisibleAdditionalModal}
            title={"Добавить товар"}
            handleClose={setVisibleAdditionalModal.bind(null, false)}
            children={myOptionsBlock}
            closeLabel={"Продолжить"}
            hideAccept={true}
          />
          {myOptions.length > 0 &&
            myOptions.map((item, key) => (
              <div className={"Adittional"}>
                {item.attribute.map(
                  (item, id) =>
                    item.picked && (
                      <div
                        className={"Adittional_item"}
                        onClick={pickAttribute.bind(null, key, id)}
                      >
                        <p className={"Adittional_item_name"}>{item.name}</p>
                        <img
                          src={require("../../../../public/img/trash.png")}
                          className={"Adittional_item_trash"}
                        />
                      </div>
                    )
                )}
              </div>
            ))}
          <div
            className={"row clickable"}
            onClick={setVisibleAdditionalModal.bind(null, true)}
          >
            <img
              src={require("../../../../public/img/add.png")}
              alt={"add"}
              className={"add"}
            />
            <p className={"text"}>Добавить доп. товар</p>
          </div>
        </div>
      </div>
    </div>
  );
};
