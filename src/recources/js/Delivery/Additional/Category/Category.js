import React, { useState, useEffect } from "react";

export const Category = ({
  item,
  createItem,
  changeType,
  deleteCat,
  deleteAttribute,
  changeAdditional,
}) => {
  const [isVisibleAddImage, setVisibleAddImage] = useState(false);
  const [cost, setCost] = useState("");
  const [name, setName] = useState("");
  const [isChangeType, setIsChangeType] = useState(false);
  const [isChangeVisibleAddImage, setChangeVisibleAddImage] = useState(false);

  const [changedName, setChangedName] = useState("");
  const [changedCost, setChangedCost] = useState("");

  const [editable, setEditable] = useState(-1);

  useEffect(() => {
    setChangedName("");
    setChangedCost("");
  }, [editable]);

  useEffect(() => {
    if (changedCost.length > 0 && changedName.length > 0)
      setChangeVisibleAddImage(true);
    else setChangeVisibleAddImage(false);
  }, [changedName, changedCost]);

  const editing = (value, field) => {
    if (field === "name") setName(value);
    else if (field === "cost") setCost(value);
  };

  useEffect(() => {
    if (cost.length > 0 && name.length > 0) setVisibleAddImage(true);
    else setVisibleAddImage(false);
  }, [cost, name]);

  const addAdditional = () => {
    createItem(name, cost);
    setCost("");
    setName("");
  };

  return (
    <div className={"Additional_cat"}>
      <div className={"Additional_cat_nameBlock"}>
        <div className={"Additional_cat_nameBlock_name"}>
          <p className={"Additional_cat_nameBlock_name_name"}>{item.name}</p>
          <p
            className={"Additional_cat_nameBlock_name_action"}
            onClick={() => {
              setIsChangeType(!isChangeType);
            }}
          >
            {item.type === 1 ? "(Один вариант)" : "(Несколько вариантов)"}
          </p>
          <img
            src={require("../../../../../public/img/trash.png")}
            className={"Additional_cat_nameBlock_name_trash"}
            onClick={deleteCat}
          />
        </div>
        {isChangeType && (
          <div className={"Additional_cat_nameBlock_changeType"}>
            <p onClick={changeType.bind(null, 1)}>Один вариант</p>
            <p onClick={changeType.bind(null, 2)}>Несколько вариантов</p>
          </div>
        )}
      </div>

      <div className={"Additional_cat_table"}>
        <div className={"Additional_cat_table_tr"}>
          <div
            className={
              "Additional_cat_table_tr_td Additional_cat_table_tr_td_green"
            }
          >
            Название
          </div>
          <div
            className={
              "Additional_cat_table_tr_td Additional_cat_table_tr_td_green"
            }
          >
            Цена
          </div>
          <div
            className={
              "Additional_cat_table_tr_td Additional_cat_table_tr_td_green"
            }
          >
            Старая цена
          </div>
        </div>

        {item.attribute.map((attribute, key) => (
          <>
            <div className={"Additional_cat_table_tr"}>
              {editable === key ? (
                <>
                  <div className={"Additional_cat_table_tr_td"}>
                    <input
                      placeholder={attribute.name}
                      value={changedName}
                      onChange={(e) => setChangedName(e.target.value)}
                      className={"Additional_cat_table_tr_td_input"}
                    ></input>
                  </div>
                  <div className={"Additional_cat_table_tr_td"}>
                    <input
                      placeholder={attribute.price}
                      type={"number"}
                      value={changedCost}
                      onChange={(e) => setChangedCost(e.target.value)}
                      className={"Additional_cat_table_tr_td_input"}
                    ></input>
                  </div>
                  {isChangeVisibleAddImage && (
                    <img
                      onClick={changeAdditional.bind(
                        null,
                        key,
                        attribute.id,
                        changedName,
                        changedCost
                      )}
                      src={require("../../../../../public/img/add.png")}
                      className={"Additional_add_image Center"}
                      alt={""}
                    />
                  )}
                </>
              ) : (
                <>
                  <div
                    className={"Additional_cat_table_tr_td"}
                    onClick={() => {
                      setEditable(key);
                    }}
                  >
                    {attribute.name}
                  </div>
                  <div
                    className={"Additional_cat_table_tr_td"}
                    onClick={() => {
                      setEditable(key);
                    }}
                  >
                    {attribute.price}
                  </div>
                  <div
                    className={"Additional_cat_table_tr_td"}
                    onClick={() => {
                      setEditable(key);
                    }}
                  >
                    {attribute.price}
                  </div>
                  <img
                    onClick={deleteAttribute.bind(null, key, attribute.id)}
                    src={require("../../../../../public/img/trash.png")}
                    className={"Additional_cat_table_tr_trash Center"}
                    alt={""}
                  />
                </>
              )}
            </div>
            <div className={"Additional_cat_table_splitter"} />
          </>
        ))}

        <div className={"Additional_cat_table_tr"}>
          <div className={"Additional_cat_table_tr_td"}>
            <input
              placeholder={"Название"}
              value={name}
              onChange={(e) => editing(e.target.value, "name")}
              className={"Additional_cat_table_tr_td_input"}
            ></input>
          </div>
          <div className={"Additional_cat_table_tr_td"}>
            <input
              placeholder={"Цена"}
              type={"number"}
              value={cost}
              onChange={(e) => editing(e.target.value, "cost")}
              className={"Additional_cat_table_tr_td_input"}
            ></input>
          </div>
          {isVisibleAddImage && (
            <img
              onClick={addAdditional}
              src={require("../../../../../public/img/add.png")}
              className={"Additional_add_image Center"}
              alt={""}
            />
          )}
        </div>
        <div className={"Additional_cat_table_splitter"} />
      </div>
    </div>
  );
};
