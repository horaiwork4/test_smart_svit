import React, { useState, useEffect } from "react";

import "./styles/styles.scss";
import { Category } from "./Category/Category";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { getApi } from "../../../../services/API";

export const Additional = ({ company }) => {
  const api = getApi().getDelivery()
  const [categorys, setCategorys] = useState([]);
  const [isTemplateActive, setTemplateActive] = useState(false);
  const [templateName, setTemplateName] = useState("");
  const dispatch = useDispatch();

  const _getAttributes = () => {
    api.getAttributes(company.id).then((data) => {
      setCategorys(data);
    });
  };

  useEffect(() => {
    if (company) {
      _getAttributes();
      dispatch(changeIsActivePage(true));
    }
  }, [company, dispatch]);

  const createNewCategory = () => {
    setTemplateName("");
    setTemplateActive(false);
    api.createAdditionalCategory(company.id, templateName).then((data) => {
      if (data.status === "success") {
        _getAttributes();
      }
    });
  };

  const createTemplateCategory = () => {
    setTemplateActive(true);
  };

  const createAditionalItem = (catIndex, id, name, cost) => {
    api.createAdditionalProduct(company.id, id, name, cost).then((data) => {
      if (data.status === "success") {
        const attiribute = {
          name: name,
          price: cost,
        };
        categorys[catIndex].attribute.push(attiribute);
        setCategorys([...categorys]);
      }
    });
  };

  // const changeType = (key, id, type) => {
  //   updateAttributeGroup(company.id, id, categorys[key].name, type).then((data) => {
  //     if (data.status === "success") {
  //       categorys[key].type = type;
  //       setCategorys([...categorys]);
  //     }
  //   });
  // };

  // const deleteAttribute = (catKey, catId, attrKey, attrId) => {
  //   fDeleteAttribute(company.id, attrId).then((data) => {
  //     console.log(data);
  //     if (data.status === "success") {
  //       categorys[catKey].attribute.splice(attrKey, 1);
  //       setCategorys([...categorys]);
  //     }
  //   });
  // };

  // const deleteCat = (catKey, catId) => {
  //   deleteAttributeGroup(company.id, catId).then((data) => {
  //     console.log(data);
  //     if (data.status === "success") {
  //       categorys.splice(catKey, 1);
  //       setCategorys([...categorys]);
  //     }
  //   });
  //   console.log(catKey, catId);
  // };

  const changeAdditional = (catKey, catId, attrKey, attrId, changedName, changedCost) => {
    console.log(catKey, catId, attrKey, attrId, changedName, changedCost);
  };

  return (
    <div className={""}>
      <div className={"Additional_add"} onClick={createTemplateCategory}>
        <img
          src={require("../../../../public/img/add.png")}
          className={"Additional_add_image"}
          alt={""}
        />
        <p className={"Additional_add_text"}>добавить категорию</p>
      </div>

      {isTemplateActive && (
        <div className={"Additional_add"}>
          <input
            placeholder={"name"}
            value={templateName}
            onChange={(e) => setTemplateName(e.target.value)}
          ></input>
          <img
            src={require("../../../../public/img/add.png")}
            style={{
              marginLeft: "10px",
            }}
            alt={""}
            className={"Additional_add_image"}
            onClick={createNewCategory}
          />
        </div>
      )}

      <div className={"Additional"}>
        {categorys.length > 0 &&
          categorys.map((item, key) => (
            <Category
              item={item}
              key={key}
              createItem={createAditionalItem.bind(null, key, item.id)}
              // changeType={changeType.bind(null, key, item.id)}
              // deleteCat={deleteCat.bind(null, key, item.id)}
              // deleteAttribute={deleteAttribute.bind(null, key, item.id)}
              changeAdditional={changeAdditional.bind(null, key, item.id)}
            />
          ))}
      </div>
    </div>
  );
};
