import React, { useEffect } from "react";
import { Orders } from "./Orders";
import { SimpleOrder } from "./SimpleOrder/SimpleOrder";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { Switch, Route, useHistory } from "react-router-dom";

export const OrdersBlock = ({ company }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    history.listen((location, action) => {
      if (location.pathname === "/orders") dispatch(changeIsActivePage(false));
    });
  }, [history, dispatch]);

  return (
    <Switch>
      <Route exact path={"/orders"}>
        <Orders company={company} />
      </Route>
      <Route exact path={"/orders/details-order"}>
        <SimpleOrder company={company} />
      </Route>
    </Switch>
  );
};
