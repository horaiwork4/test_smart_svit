import React, { useState, useEffect } from "react";
import { OrderInfoCard } from "./OrderInfoCard";
import { ItemTile } from "../ItemTile";
import "../styles/Delivery.scss";
import { getApi } from "../../../../services/API";
import { Loader } from "../../../common/js/components/Loader";

export function Orders({ company }) {
  const api = getApi().getFood();
  const dataOfstatus = [
    { id: 10, name: "Все" },
    // { id: 15, name: "Новый" },
    { id: 1, name: "Оформлен" },
    { id: 2, name: "Обработка" },
    { id: 3, name: "В пути" },
    { id: 4, name: "Выполнено" },
  ];

  const [orders, setOrders] = useState(null);
  const [pickedCatOrders, setPickedCatOrders] = useState([]);
  const [pickedCat, setPickedCat] = useState(10);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    fillPage();
    // eslint-disable-next-line
  }, [company]);

  useEffect(() => {
    if (orders) {
      if (pickedCat === 10) {
        setPickedCatOrders(orders);
        return;
      }
      const ordersF = orders.filter((order) => +order.order_status === dataOfstatus[pickedCat].id);
      setPickedCatOrders(ordersF);
    }
  }, [pickedCat, orders]);

  useEffect(() => {
    console.log(pickedCatOrders);
  }, [pickedCatOrders]);

  const [orderID, setOrderID] = useState(null);

  const [isVisibleModal, setVisibleModal] = useState(false);

  const handlePressStatus = (index) => {
    setPickedCat(dataOfstatus[index].id);
  };

  const fillPage = () => {
    if (!!company && !!company.id) {
      setLoading(true);
      api.getCompanyOrders(company.id).then((data) => {
        if (!!data && !!data.data) {
          const myData = data.data.map((item) => {
            const myOrder = { ...item };
            myOrder.visible = false;

            return myOrder;
          });

          setOrders([...myData]);
          setLoading(false);
        }
      });
    }
  };

  function onChangeStatus(index) {
    if (!!orders && !!orders[index] && !!orders[index].id) {
      setOrderID(orders[index].id);
    }
    setVisibleModal(true);
  }

  function hideStatusModal() {
    setVisibleModal(false);
  }

  function handleChangeStatus(order_status) {
    if (!!order_status) {
      api.changeOrderStatus(+company.id, +order_status, +orderID).then((data) => {
        hideStatusModal();
        fillPage();
      });
    }
  }

  return (
    <>
      <ItemTile
        category={dataOfstatus}
        handlePress={handlePressStatus}
        visibleButtons={false}
        ordersStatus={true}
        pickedId={pickedCat}
      />
      <div className={"OrdersBlock"}>
        <OrderInfoCard
          company={company}
          orders={pickedCatOrders}
          onChangeStatusButton={onChangeStatus}
          visibleModal={isVisibleModal}
          handleChangeStatus={handleChangeStatus}
          hideStatusModal={hideStatusModal}
        />
      </div>
      {isLoading && <Loader />}
    </>
  );
}
