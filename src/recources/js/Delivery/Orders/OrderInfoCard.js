import React, { useState, Fragment } from "react";
import "../styles/Delivery.scss";
import { Button } from "../../../common/js/components/Button";
import { Modal } from "../../../common/js/components/Modal";
import { Link } from "react-router-dom";

const options = [
  {
    key: 1,
    text: "Оформлен",
  },
  {
    key: 2,
    text: "Обрабатывается",
  },
  {
    key: 3,
    text: "Доставляется",
  },
  {
    key: 4,
    text: "Выполнен",
  },
];

export const OrderInfoCard = ({
  company,
  orders,
  onChangeStatusButton,
  visibleModal,
  handleChangeStatus,
  hideStatusModal,
}) => {
  const [value, setValue] = useState(null);

  function handleSelectStatus(value) {
    if (!!value) {
      handleChangeStatus(value);
      hideStatusModal();
    }
  }

  const pickOrder = (order) => {
    localStorage.setItem("pickedOrder", JSON.stringify(order));
  };

  const status = (
    <div>
      {options.map((item) => {
        return (
          <div key={item.key} className={"buttonContainer"}>
            <p>{item.text}</p>
            <button className={"circle"} onClick={() => setValue(item.key)}>
              {value === item.key && <div className={"checkedCircle"}></div>}
            </button>
          </div>
        );
      })}
    </div>
  );

  const insideOrders = !!orders
    ? orders.map((item, index) => {
        return (
          <Fragment key={index}>
            <div className={"Order"}>
              <div className={"Order__top"}>
                {!!item &&
                !!item.orders &&
                !!item.orders.basket &&
                !!item.orders.basket.user &&
                !!item.orders.basket.user.avatar ? (
                  <img
                    className={"Order__top-avatar"}
                    src={
                      "http://smartsvit.shn-host.ru" +
                      item.orders.basket.user.avatar
                    }
                    alt={"avatar"}
                  />
                ) : null}
                {!!item &&
                !!item.orders &&
                !!item.orders.basket &&
                !!item.orders.basket.user &&
                !!item.orders.basket.user.first_name &&
                !!item.orders.basket.user.last_name ? (
                  <Link
                    to={"/orders/details-order"}
                    onClick={pickOrder.bind(null, item)}
                    className={"Dish__mainInfo"}
                  >
                    <div className={"Order__top-nameprice"}>
                      <p
                        className={"Order__user"}
                      >{`${item.orders.basket.user.first_name} ${item.orders.basket.user.last_name}`}</p>
                      <p className={"Order__price"}> {item.total_price} ₴ </p>
                    </div>
                  </Link>
                ) : null}

                {!!item && !!item.status && !!item.status.name ? (
                  <Button
                    type={"submit"}
                    label={item.status.name}
                    className={"Order__statusbutton"}
                    onCLick={() => onChangeStatusButton(index)}
                  />
                ) : null}
              </div>
              {!!item &&
              !!item.orders &&
              !!item.orders.items &&
              !!item.orders.items[0] &&
              !!item.orders.items[0].order_id &&
              !!item.orders.address ? (
                <div className={"Order__bottom"}>
                  <p className={"Order__order"}>
                    № заказа: {item.orders.items[0].order_id}{" "}
                  </p>
                  <p className={"Order__location"}>
                    Адрес: {item.orders.address}{" "}
                  </p>
                </div>
              ) : null}

              <div className={"Order__separator"}></div>

              <Modal
                handleClose={hideStatusModal}
                show={visibleModal}
                children={status}
                title={"Выбрать статус"}
                onAccept={handleSelectStatus.bind(null, value)}
              />
            </div>
          </Fragment>
        );
      })
    : null;

  return !!orders ? insideOrders : <div>Входящих заказов нет</div>;
};
