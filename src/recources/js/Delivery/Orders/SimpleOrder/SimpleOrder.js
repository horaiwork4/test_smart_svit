import React, { useState, useEffect } from "react";
import "./styles/simpleOrder.scss";

export function SimpleOrder({ company }) {
  const [downloaded, setDownLoaded] = useState(false);

  const [totalPrice, setTotalPrice] = useState("");
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [numOfOrder, setNumOfOrder] = useState("");
  const [comment, setComment] = useState("");
  const [status, setStatus] = useState("");

  const [products, setProducts] = useState(null);

  useEffect(() => {
    if (!!company && !downloaded) {
      setDownLoaded(true);
      let order = JSON.parse(localStorage.getItem("pickedOrder"));
      if (order) {
        setName(order.orders.basket.user.first_name);
        setSurname(order.orders.basket.user.last_name);
        setTotalPrice(order.total_price);
        setPhone(order.orders.phone);
        setAddress(order.orders.address);
        setComment(order.orders.comment);
        setNumOfOrder(order.delivery.id);
        setStatus(order.status.name);
        setProducts(order.orders.items);
      }
    }
  }, [company]);

  const personalInfo = (
    <div className={"PersonContainer"}>
      <div className={"Person"}>
        <div className={"Person__wrapper-avatar"}>
          <img
            className={"Person__avatar"}
            src={require("../../../../../public/img/user_order.png")}
            alt="user"
          />
        </div>
        <div className={"Person__wrapper-nameprice"}>
          <p className={"Person__wrapper-nameprice-name"}>
            {name} {surname}
          </p>
          <p className={"Person__wrapper-nameprice-price"}>{totalPrice}₴</p>
        </div>
        <button className={"Person__btn"}>{status}</button>
      </div>
      <div className={"Details"}>
        <p className={"Details__num"}>
          <span className={"Details__num-lbl"}> № заказа:</span> {numOfOrder}
        </p>
        <p className={"Details__label"}>Адрес</p>
        <p className={"Details__label-addres input"}>{address}</p>
        <div className={"Details__separator"}></div>
        <p className={"Details__label"}>Номер телефона</p>
        <p className={"Details__label-phone input"}>{phone}</p>
        <div className={"Details__separator"}></div>
        <p className={"Details__label"}>Комментарий</p>
        <p className={"Details__label-comment input"}>{comment}</p>
      </div>
    </div>
  );

  const productsInfo =
    products &&
    products.map((item) => {
      return (
        <div className={"Product"}>
          <div className={"Product__imgWrapper"}>
            {item && item.product && item.product.images[0] && item.product.images[0].url && (
              <img
                className={"Product__img"}
                src={"http://smartsvit.chost.com.ua" + item.product.images[0].url}
              />
            )}
          </div>
          <div className={"Product__rightSide"}>
            <div className={"Product__info"}>
              {item && item.product && item.product.name && (
                <p className={"Product__info-name"}> {item.product.name} </p>
              )}
              {item && item.product && item.product.vendor_code && (
                <p className={"Product__info-vendor"}> Артикул: {item.product.vendor_code}</p>
              )}
            </div>
            <div className={"Product__quantprice"}>
              {item && item.quantity && (
                <p className={"Product__quantprice-quant"}>x{item.quantity}</p>
              )}
              {item && item.product && item.product.price && (
                <p className={"Product__quantprice-price"}> {item.product.price}₴</p>
              )}
            </div>
          </div>
        </div>
      );
    });

  return (
    <div className={"SimpleOrdersBlock"}>
      {personalInfo} <div className={"ProductsContainer"}> {productsInfo}</div>
    </div>
  );
}
