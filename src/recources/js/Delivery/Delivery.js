import React, { useEffect, useState } from "react";
import "./styles/Delivery.scss";
import { DishesBlock } from "./Dishes/DishesBlock";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../store/actions/PrewPage";
import { Switch, Route, useHistory } from "react-router-dom";
import { Additional } from "./Additional/Additional";
import { NewProduct } from "./NewProduct/NewProduct";
import { setHeaderSaveBtn } from "../../../store/actions/actions";
import { DeliveryInfo } from "./DeliveryInfo/DeliveryInfo";

export const Delivery = ({ company }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  const [pickedCategoryId, setPickedCategoryId] = useState(null);

  useEffect(() => {
    history.listen((location, action) => {
      if (location.pathname === "/delivery") {
        dispatch(changeIsActivePage(false));
        dispatch(setHeaderSaveBtn(null));
      }
    });
  }, [history, dispatch]);

  return (
    <Switch>
      <Route exact path={"/delivery"}>
        <DishesBlock
          company={company}
          pickedCategoryId={pickedCategoryId}
          setPickedCategoryId={setPickedCategoryId}
        />
      </Route>
      <Route exact path={"/delivery/additional"}>
        <Additional company={company} />
      </Route>
      <Route exact path={"/delivery/newProduct"}>
        <NewProduct company={company} pickedCategoryId={pickedCategoryId} />
      </Route>
      <Route exact path={"/delivery/delivery-info"}>
        <DeliveryInfo company={company} />
      </Route>
    </Switch>
  );
};
