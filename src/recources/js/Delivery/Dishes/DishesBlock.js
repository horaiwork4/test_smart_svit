// /* eslint-disable no-unused-vars */
import React, { useState, useEffect } from "react";
import "../styles/Dishes.scss";
import { ItemTile } from "../ItemTile";
import { Dishes } from "./Dishes";
import { Button } from "../../../common/js/components/Button";
import { Link } from "react-router-dom";
import { getApi } from "../../../../services/API";
import { Loader } from "../../../common/js/components/Loader";

export function DishesBlock({
  company,
  pickedCategoryId,
  setPickedCategoryId,
}) {
  const api = getApi().getFood();
  const [category, setCategory] = useState([]);
  const [isPicked, setIsPicked] = useState(false);
  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    _getCompanysCategories();
  }, [company]);

  const _deleteProduct = (pr_id) => {
    setLoading(true);
    api.deleteProduct(company.id, pr_id).then((data) => {
      if (data.status === "success") {
        const item = localStorage.getItem("picked_category");
        category.forEach((_item, id) => {
          if (_item.id === +item) {
            getCompanyProducts(category[id].id);
          }
        });
        setLoading(false);
      }
    });
  };

  const clearProduct = () => {
    localStorage.removeItem("pickedProduct");
  };

  const _getCompanysCategories = () => {
    if (!!company && !!company.id) {
      setLoading(true);
      api.getCompanysCategories(company.id).then((data) => {
        if (!!data && !!data.shop && !!data.shop.categorys) {
          //выбор из local storage категории
          if (!isPicked) {
            const item = localStorage.getItem("picked_category");
            if (item === null) {
              data.shop.categorys[0].picked = true;
              localStorage.setItem(
                "picked_category",
                data.shop.categorys[0].id
              );
              setPickedCategoryId(data.shop.categorys[0].id);
              getCompanyProducts(data.shop.categorys[0].id);
            } else {
              data.shop.categorys.forEach((_item, id) => {
                if (_item.id === +item) {
                  _item.picked = true;
                  setPickedCategoryId(data.shop.categorys[id].id);
                  getCompanyProducts(data.shop.categorys[id].id);
                }
              });
            }
            setIsPicked(true);
          }
          setCategory([...data.shop.categorys]);
        }
        setLoading(false);
      });
    }
  };

  const [products, setProducts] = useState([]);

  function getCompanyProducts(cat_id) {
    setPickedCategoryId(cat_id);
    localStorage.setItem("picked_category", cat_id);

    category.forEach((element) => {
      if (element.id === cat_id) element.picked = true;
      else element.picked = false;
    });
    setCategory([...category]);
    const page = 1;
    setLoading(true);
    api.getCompanysProductFromCatId(company.id, cat_id, page).then((data) => {
      setProducts(data.data);
      setLoading(false);
    });
  }

  const deleteFoodCategory = () => {
    if (company && company.id && pickedCategoryId) {
      setLoading(true);
      api.deleteFoodCat(company.id, pickedCategoryId).then((data) => {
        if (!!data && data.status === "success") {
          _getCompanysCategories();
        }
      });
    }
  };

  return (
    <>
      <div className={"DishesTop"}>
        <ItemTile
          category={category}
          company={company}
          handlePress={getCompanyProducts}
          visibleButtons={true}
          ordersStatus={false}
          pickedId={pickedCategoryId}
          getCompanysCategories={_getCompanysCategories}
        />
      </div>
      <div className={"Dishes__actions"}>
        <div className={"Dishes__actions-addWrapper"}>
          <img
            src={require("../../../../public/img/add_prod.png")}
            alt={"add_prod"}
            className={"Dishes__actions-add-img"}
          />
          <Link to={"/delivery/newProduct"} onClick={clearProduct}>
            <Button
              className={"Dishes__actions-add-product"}
              label={"Добавить товар"}
            />
          </Link>
        </div>
        {category && category.length > 0 && (
          <div className={"Dishes__actions-removeWrapper"}>
            <img
              src={require("../../../../public/img/rm_cat.png")}
              className={"Dishes__actions-rm-img"}
              alt={"rm_cat"}
            />
            <Button
              className={"Dishes__actions-rm-category"}
              label={"Удалить категорию"}
              onCLick={deleteFoodCategory}
            />
          </div>
        )}
      </div>
      {!!category && category.length > 0 ? (
        <div className={"DishesBlock"}>
          <Dishes data={products} deleteProduct={_deleteProduct} />
        </div>
      ) : (
        <div>Товары отсутствуют</div>
      )}
      {isLoading && <Loader />}
    </>
  );
}
