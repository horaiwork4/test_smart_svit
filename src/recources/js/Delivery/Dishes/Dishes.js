import React from "react";
import "../styles/Dishes.scss";
import { Link } from "react-router-dom";

export function Dishes({ data, deleteProduct }) {
  const pickProduct = (product) => {
    localStorage.setItem("pickedProduct", JSON.stringify(product));
  };
  if (data && data.length > 0) {
    return data.map((item) => {
      return (
        <div className="Dish" key={item.id}>
          <Link
            to={"/delivery/newProduct"}
            onClick={pickProduct.bind(null, item)}
            className={"Dish__mainInfo"}
          >
            <div className={"Dish__imageWrapper"}>
              <img
                className={"Dish__image"}
                src={"http://smartsvit.chost.com.ua" + item.images[0].url}
                alt={"dish"}
              />
            </div>
            <div className={"Dish__info"}>
              <div className={"Dish__title"}>{item.name}</div>
              {item.vendor_code && (
                <div className={"Dish__subtitle"}>Артикул: {item.vendor_code}</div>
              )}
              <div className={"Dish__price"}>Цена: {item.price} грн.</div>
            </div>
          </Link>
          <div className={"Dish__action"}>
            <button className={"Dish__delete"}>
              <img
                className={"Dish__delete-icon"}
                onClick={deleteProduct.bind(null, item.id)}
                src={require("../../../../public/img/trash.png")}
                alt={"trash"}
              />
            </button>
          </div>
        </div>
      );
    });
  } else return <></>;
}
