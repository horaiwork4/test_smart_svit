import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { SideMenu } from "../SideMenu/SideMenu";
import { Switch, Route } from "react-router-dom";

import "./styles/MainPage.scss";
import { Settings } from "../Settings/Settings";
import { Chats } from "../Chats/Chats";
import { Clients } from "../Clients/Clients";
import { Header } from "../Header/Header";
import { News } from "../News/News";
import { Delivery } from "../Delivery/Delivery";
import { Orders } from "../Delivery/Orders/Orders";
import { OrdersBlock } from "../Delivery/Orders/OrdersBlock";
import { Sale } from "../Sale/Sale";
// import { Additional } from "../Delivery/Additional/Additional";
// import { AdvertismentBlock } from "../Advertisment/AdvertismentBlock";
import { Advertisment } from "../Advertisment/Advertisment";

export const MainPage = () => {
  const [company, setCompany] = useState(null);

  const history = useHistory();

  const isCompanyExist = () => {
    if (!localStorage.getItem("company") || !localStorage.getItem("token")) {
      history.push("/login");
    }
  };

  useEffect(() => {
    isCompanyExist();
    setCompany(JSON.parse(localStorage.getItem("company")));
    // eslint-disable-next-line
  }, []);

  return (
    <div className={"Main"}>
      <SideMenu />
      <div className={"Main_content"}>
        <Header company={company} />
        <Switch>
          <Route exact path={"/"}>
            <News company={company} />
          </Route>
          <Route path={"/settings"}>
            <Settings />
          </Route>
          <Route path={"/chats"}>
            <Chats company={company} />
          </Route>
          <Route path={"/clients"}>
            <Clients company={company} />
          </Route>
          <Route path={"/delivery"}>
            <Delivery company={company} />
          </Route>
          <Route path={"/orders"}>
            <OrdersBlock company={company} />
          </Route>
          <Route path={"/sale"}>
            <Sale company={company} />
          </Route>
          <Route path={"/adv"}>
            <Advertisment company={company} />
          </Route>
        </Switch>
      </div>
    </div>
  );
};
