import React, { useState, useEffect } from "react";
import "../Sale/styles/Ads.scss";
import { CardItem } from "../Components/CardItem/CardItem";
import { Link } from "react-router-dom";
import { getApi } from "../../../services/API";
import { Loader } from "../../common/js/components/Loader";

export const AdvertismentBlock = ({ company }) => {
  const [adv, setAdv] = useState(null);
  const api = getApi().getAdv();

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (!!company && !!company.id) {
      setLoading(true);
      api.getListAdvertisment(company.id).then((data) => {
        setAdv(data.data);
        setLoading(false);
      });
    }
  }, [company]);

  const handleDeleteAdv = (_item) => {
    setLoading(true);
    api.deleteAdv(company.id, _item.board.id).then((data) => {
      if (data.status === "success") {
        setAdv(adv.filter((item) => item.board.id !== _item.board.id));
        setLoading(false);
      }
    });
  };

  return (
    <>
      <div className={"SalesAddBlock"}>
        <img
          className={"SalesAddBlock__img"}
          alt={"add"}
          src={require("../../../public/img/add_promo.png")}
        />
        <Link to={"/adv/newAdv"}>
          <button className={"SalesAddBlock__text"}>Создать объявление</button>
        </Link>
      </div>
      {isLoading && <Loader />}
      {!!adv && adv.length > 0 ? (
        <div className={"SalesBlock"}>
          <CardItem items={adv} isPromo={true} handleDeleteItem={handleDeleteAdv} />
        </div>
      ) : (
        !isLoading && <div className={"SalesBlock__apsentAdv"}>Объявлений нету</div>
      )}
    </>
  );
};
