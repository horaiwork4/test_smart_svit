import React, { useState, useEffect } from "react";
import GoogleMapReact from "google-map-react";

import { getApi } from "../../../../services/API";
import { sectorsListService } from "../../../../services/advertisment/advService";

const API_KEY = "AIzaSyA2QiCinZN-Kf4LdPVmzCPq95Sm0IuWdXU";

export function AppMapView({ handleChoosenSectors }) {
  const [sectors, setSectors] = useState(null);
  const [map, setMap] = useState(false);
  const [maps, setMaps] = useState(false);
  const [markers, setMarkers] = useState([]);
  let api = getApi().getAdv();

  // const [choosenSectors, setChoosenSectors] = useState(null);

  // useEffect(() => {
  //   if (!!choosenSectors) {
  //     handleChoosenSectors(choosenSectors);
  //   }
  // });

  useEffect(() => {
    api.sectorsListService().then((data) => {
      const newSectors = data.map((el) => {
        const newArea = JSON.parse(el.area.area).map((item) => {
          const newItem = {
            lat: item.lat || item.latitude,
            lng: item.lng || item.longitude,
          };

          return newItem;
        });

        el.area = newArea;

        el.tapped = false;

        return el;
      });

      setSectors([...newSectors]);
    });
  }, []);

  const clearMarkers = () => {
    markers.forEach((marker) => {
      marker.setMap(null);
    });
    setMarkers([]);
  };

  useEffect(() => {
    if (maps && map && sectors) {
      sectors.map((el, id) => {
        if (!el.strokeColor) {
          el.strokeColor = "dodgerblue";
          el.fillColor = "dodgerblue";
        }

        const newArea = new maps.Polygon({
          paths: el.area,
          strokeColor: el.strokeColor,
          strokeWidth: 2,
          strokeWeight: 2,
          fillColor: el.fillColor,
          fillOpacity: 0.35,
          clickable: true,
        });
        markers.push(newArea);

        newArea.addListener("click", handleClickSector.bind(null, id));

        newArea.setMap(map);
      });
      setMarkers([...markers]);
    }
  }, [map, maps, sectors]);

  const distanceToMouse = (pt, mousePos) => {
    if (pt && mousePos) {
      return Math.sqrt(
        (pt.x - mousePos.x) * (pt.x - mousePos.x) + (pt.y - mousePos.y) * (pt.y - mousePos.y)
      );
    }
  };

  const handleClickSector = (sectorId) => {
    sectors[sectorId].strokeColor =
      sectors[sectorId].strokeColor === "dodgerblue" ? "blue" : "dodgerblue";
    sectors[sectorId].fillColor =
      sectors[sectorId].fillColor === "dodgerblue" ? "blue" : "dodgerblue";
    sectors[sectorId].picked = !sectors[sectorId].picked;
    const choosen = sectors.filter((sector) => sector.picked === true);

    handleChoosenSectors(choosen);
    clearMarkers();
    setSectors([...sectors]);
  };

  const onGoogleApiLoaded = (map, maps, index) => {
    setMap(map);
    setMaps(maps);
  };

  return (
    <div style={{ width: "50rem", height: "37rem" }}>
      <GoogleMapReact
        bootstrapURLKeys={{ key: API_KEY, language: "ru" }}
        defaultCenter={{ lat: 50.4500336, lng: 30.5241361 }}
        defaultZoom={11}
        distanceToMouse={distanceToMouse}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={({ map, maps }) => onGoogleApiLoaded(map, maps)}
      />
    </div>
  );
}
