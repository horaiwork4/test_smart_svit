import React, { useEffect } from "react";
import { AdvertismentBlock } from "./AdvertismentBlock";
import { NewAdvertisment } from "./NewAdvertisment/NewAdvertisment";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../store/actions/PrewPage";
import { Switch, Route, useHistory } from "react-router-dom";

export const Advertisment = ({ company }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    history.listen((location, action) => {
      if (location.pathname === "/adv") dispatch(changeIsActivePage(false));
    });
  }, [history, dispatch]);

  return (
    <Switch>
      <Route exact path={"/adv"}>
        <AdvertismentBlock company={company} />
      </Route>
      <Route exact path={"/adv/newAdv"}>
        <NewAdvertisment company={company} />
      </Route>
    </Switch>
  );
};
