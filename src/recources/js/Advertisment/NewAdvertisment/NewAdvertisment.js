/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useRef, useDebugValue } from "react";
import { ImageLoader } from "../../Components/ImageLoader/ImageLoader";
import { Input } from "../../Components/Input/Input";
import "./styles.scss";
import { AppMapView } from "../AppMapView/AppMapView";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { setHeaderSaveBtn } from "../../../../store/actions/actions";
import { useHistory } from "react-router-dom";
import { getApi } from "../../../../services/API";
import { Loader } from "../../../common/js/components/Loader";

export function NewAdvertisment({ company }) {
  const api = getApi().getAdv();

  const [name, setName] = useState("");
  const [nameError, setNameError] = useState("");

  const [desc, setDesc] = useState("");
  const [descError, setDescError] = useState("");

  const [typeAd, setTypeAd] = useState(null);

  const [photos, setPhotos] = useState([]);
  const [rawPhotos, setRawPhotos] = useState([]);
  const [photosError, setPhotosError] = useState([]);

  const [adId, setAdId] = useState(null);

  const [advSectors, setAdvSectors] = useState([]);

  const [isLoading, setLoading] = useState(false);

  const history = useHistory();

  const dispatch = useDispatch();
  const fileRef = useRef();

  useEffect(() => {
    dispatch(changeIsActivePage(true));
    dispatch(
      setHeaderSaveBtn(createAdv.bind(null, advSectors, name, desc, typeAd, adId, rawPhotos))
    );
  }, [dispatch]);

  useEffect(() => {
    dispatch(
      setHeaderSaveBtn(createAdv.bind(null, advSectors, name, desc, typeAd, adId, rawPhotos))
    );
  }, [advSectors, name, desc, typeAd, adId, rawPhotos]);

  useEffect(() => {
    if (!!company && !!company.id) {
      api.createAdFirstStep(company.id).then((data) => {
        setAdId(data.board_id);
      });
    }
  }, [company, dispatch]);

  //sectors
  const handleChoosenSectors = (data) => {
    setAdvSectors(data);
  };

  //create adv
  const createAdv = (sectors, name, desc, typeAd, adId, images) => {
    let error = false;

    if (name.length < 9) {
      error = true;
      setNameError(true);
    }

    if (desc.length < 19) {
      error = true;
      setDescError(true);
    }

    if (images.length === 0) {
      error = true;
      setPhotosError(true);
    }

    if (sectors.length === 0) {
      error = true;
    }

    if (!typeAd || !adId) {
      error = true;
    }

    if (!error) {
      setLoading(true);
      let houses = [];
      if (sectors && sectors.length > 0)
        houses = sectors.map((sector) => {
          return JSON.parse(sector.house_id);
        });

      houses = [].concat(...houses);

      let photos = [];
      if (images.length) {
        images.forEach((image) => {
          photos.push(uploadPhotos(image));
        });
      }

      Promise.all(photos).then((images) => {
        api.createAdSecondStep(adId, images, typeAd, houses, name, desc).then((data) => {
          console.log(data);
          if (data.status === "success") {
            setLoading(false);
            history.goBack();
          }
        });
      });
    } else {
      alert("Fill all fields please");
    }
  };

  // photo logic
  const uploadPhotos = (image) => {
    return new Promise((resolve, reject) => {
      api.uploadPhoto(image).then((data) => {
        resolve(data.data.url);
      });
    });
  };

  const addPhoto = (_photos) => {
    if (_photos.length > 0) {
      rawPhotos.push(_photos[_photos.length - 1]);
      setRawPhotos([...rawPhotos]);

      let path = URL.createObjectURL(_photos[0]);
      photos.push(path);
      setPhotos([...photos]);
    }
  };
  //

  return (
    <div className={"NewAdv"}>
      <div className={"NewAdv__infoSection"}>
        <div className={"NewAdv__photos"}>
          {photos.map((item, key) => (
            <ImageLoader photo={item} key={key} alt={"photo"} />
          ))}
          <label>
            <ImageLoader error={photosError} />
            <input
              type="file"
              id={"files"}
              ref={fileRef}
              accept=".jpg, .jpeg, .png"
              onChange={(e) => addPhoto(e.currentTarget.files)}
              style={{ display: "none" }}
            />
          </label>
        </div>
        <div className={"NewAdv__nameadv"}>
          <Input
            name={"Название объявления"}
            defaultValue={"Название объявления *"}
            helperText={`Минимум символов ${name.length}/10`}
            value={name}
            setValue={setName}
            error={nameError}
            withEndLine={true}
            type={"text"}
          />
        </div>
        <div className={"NewAdv__descadv"}>
          <Input
            name={"Описание"}
            defaultValue={"Описание *"}
            helperText={`Минимум символов ${desc.length}/20`}
            multiLine={true}
            value={desc}
            error={descError}
            setValue={setDesc}
            withEndLine={true}
            type={"text"}
          />
        </div>
        <div className={"NewAdv__typeBlock"}>
          <p className={"NewAdv__typeBlock-title"}>
            Тип объявления{" "}
            <span className={"NewAdv__typeBlock-title-thin"}>
              {!!typeAd && typeAd === 1
                ? "Недвижимость"
                : !!typeAd && typeAd === 2
                ? "Информационный"
                : !!typeAd && typeAd === 3
                ? "Вакансии"
                : "(Только 1)"}
            </span>
          </p>
          <div className={"NewAdv__typeBlock-btns"}>
            <button className={"NewAdv__typeBlock-btns-btn"} onClick={() => setTypeAd(1)}>
              Недвижимость
            </button>
            <button className={"NewAdv__typeBlock-btns-btn"} onClick={() => setTypeAd(2)}>
              Информационный
            </button>
            <button className={"NewAdv__typeBlock-btns-btn"} onClick={() => setTypeAd(3)}>
              Вакансии
            </button>
          </div>
        </div>
      </div>
      <div className={"NewAdv__mapBlock"}>
        <p className={"NewAdv__mapBlock-title"}>
          На карте необходимо выбрать участки, затем <br /> дома, что бы жители увидели Ваши
          объявления
        </p>
        <AppMapView handleChoosenSectors={handleChoosenSectors} />
      </div>

      {isLoading && <Loader />}
    </div>
  );
}
