import React, { useState } from "react";

import "./styles/SideMenu.scss";
import { NavLink } from "react-router-dom";

export const SideMenu = () => {
  const logout = () => {
    localStorage.clear();
  };

  const [menu, setMenu] = useState([
    {
      image: require("../../../public/img/sideMenu/enabled/main.png"),
      disImage: require("../../../public/img/sideMenu/disabled/main.png"),
      name: "Главная",
      link: "/",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/clients.png"),
      disImage: require("../../../public/img/sideMenu/disabled/clients.png"),
      name: "Мои клиенты",
      link: "/clients",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/orders_active.png"),
      disImage: require("../../../public/img/sideMenu/disabled/orders_disable.png"),
      name: "Мои заказы",
      link: "/orders",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/delivery.png"),
      disImage: require("../../../public/img/sideMenu/disabled/delivery.png"),
      name: "Доставка",
      link: "/delivery",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/chat.png"),
      disImage: require("../../../public/img/sideMenu/disabled/chat.png"),
      name: "Чаты",
      link: "/chats",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/dashboard.png"),
      disImage: require("../../../public/img/sideMenu/disabled/dashboard.png"),
      name: "Услуги",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/ads.png"),
      disImage: require("../../../public/img/sideMenu/disabled/ads.png"),
      name: "Объявления",
      link: "/adv",
    },
    {
      image: require("../../../public/img/sideMenu/enabled/promotions.png"),
      disImage: require("../../../public/img/sideMenu/disabled/promotions_dis.png"),
      name: "Акции",
      link: "/sale",
    },
  ]);

  const [bottomMenu, setBottomMenu] = useState([
    {
      image: require("../../../public/img/settings.png"),
      name: "Настройки",
      link: "/settings",
    },
    {
      image: require("../../../public/img/exit.png"),
      name: "Выход",
      link: "/login",
      onClick: logout.bind(null),
    },
  ]);

  const changeLink = (key, isBottom = false) => {
    menu.forEach((item, id) => {
      item.picked = false;
      if (isBottom && id === key) item.picked = true;
    });

    bottomMenu.forEach((item, id) => {
      item.picked = false;
      if (!isBottom && id === key) item.picked = true;
    });

    setMenu([...menu]);
    setBottomMenu([...bottomMenu]);
  };

  return (
    <div className={"Menu"}>
      <div className={"header"}>
        <img
          src={require("../../../public/img/logo.png")}
          className={"header_icon"}
          alt={"header"}
        />
        <p className={"header_text"}>Smart Svit</p>
      </div>

      <div className={"splitter"} />

      <div className={"Menu_items"}>
        {menu.map((item, key) => (
          <NavLink
            to={item.link ? item.link : "/"}
            className={"Menu_items_item"}
            activeClassName={"Menu_items_item"}
            onClick={changeLink.bind(null, key)}
            key={key}
          >
            <img
              className={"Menu_items_item_icon"}
              src={item.picked ? item.image : item.disImage}
              alt={"icon-items"}
            />
            <p
              className={
                item.picked
                  ? "Menu_items_item_name"
                  : "Menu_items_item_name Menu_items_item_name_disabled"
              }
            >
              {item.name}
            </p>
          </NavLink>
        ))}

        <div className={"Menu_items Menu_items_down"}>
          {bottomMenu.map((item, key) => (
            <NavLink
              key={key * 10}
              to={item.link ? item.link : "/"}
              className={"Menu_items_item"}
              onClick={item.onClick ? item.onClick : changeLink.bind(null, key)}
            >
              <img className={"Menu_items_item_icon"} src={item.image} alt={"item-icon"} />
              <p className={"Menu_items_item_name"}>{item.name}</p>
            </NavLink>
          ))}
        </div>
      </div>
    </div>
  );
};
