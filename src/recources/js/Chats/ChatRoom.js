import React, { useState, useEffect } from "react";
import moment from "moment";

export const ChatRoom = ({ chatInfo, companyId, openChat, isActive }) => {
  const [userFrom, setUserFrom] = useState(null);

  useEffect(() => {
    const getUserFrom = (chatInfo) => {
      chatInfo.chat_participation.map((user) => {
        if (user.company_id !== companyId) {
          user.company_id === null
            ? setUserFrom(user.user_name[0])
            : setUserFrom(user.company_name[0]);
        }
        return null;
      });
      return null;
    };

    if (chatInfo) {
      getUserFrom(chatInfo);
    }
  }, [chatInfo, companyId]);

  return (
    <div
      className={isActive ? "Chat_rooms_room Chat_rooms_room_active" : "Chat_rooms_room"}
      onClick={openChat.bind(null, chatInfo.id)}
    >
      <img
        className={"Chat_rooms_room_img"}
        src={"http://smartsvit.shn-host.ru" + (userFrom && userFrom.avatar && userFrom.avatar)}
        alt={"Chat_room"}
      />
      <div className={"Chat_rooms_room_info"}>
        <p className={"Chat_rooms_room_info_name"}>
          {userFrom && userFrom.first_name && userFrom.first_name}{" "}
          {userFrom && userFrom.last_name && userFrom.last_name}
        </p>
        <p className={"Chat_rooms_room_info_lastMessage"}>{chatInfo.message.body}</p>
      </div>
      <p className={"Chat_rooms_room_time"}>
        {moment(chatInfo.message.created_at).format("hh:mm")}
      </p>
    </div>
  );
};
