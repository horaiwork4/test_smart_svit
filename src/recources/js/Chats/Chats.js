import React, { useState, useEffect } from "react";

import "./styles/Chats.scss";
import { ChatRoom } from "./ChatRoom";
import { Messager } from "./Messager";
import { changeIsActivePage } from "../../../store/actions/PrewPage";
import { useDispatch } from "react-redux";
import { getApi } from "../../../services/API";

export const Chats = ({ company }) => {
  const api = getApi().getChat();
  const [chats, setChats] = useState([]);
  const [activeChatBoard, setActiveChatBoard] = useState(null);
  const [сhatId, setChatId] = useState(null);
  const [activeChatParticipation, setActiveChatParticipation] = useState(null);
  const [user, setUser] = useState(null);
  const [message, setMessage] = useState("");
  const [activeChatId, setActiveChatId] = useState(null);
  const [tabletChats, setTabletChats] = useState(false);
  const [files, setFiles] = useState([]);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(changeIsActivePage(false));
    if (company)
      api.getCompanyChats(company.id).then((data) => {
        let tempChats = [];
        for (let [, value] of Object.entries(data.chats)) {
          tempChats.push(value);
        }
        setChats(tempChats);
      });
  }, [company, dispatch]);

  const showChat = (key, id) => {
    if (window.innerWidth < 1200) {
      setTabletChats(true);
    }
    setChatId(key);
    showChatWithId(id);
  };

  useEffect(() => {
    if (activeChatParticipation) {
      activeChatParticipation.forEach((u) => {
        if (u.company_id !== company.id) {
          setUser(u);
        }
      });
    }
  }, [activeChatParticipation, company]);

  const showChatWithId = (id) => {
    setActiveChatId(id);
    api.getChatWithId(company.id, id).then((data) => {
      setActiveChatBoard(data.chats.chat_message);
      setActiveChatParticipation(data.chats.chat_participation);
    });
  };

  const _sendMessage = () => {
    _uploadFiles();
    if (message.length > 0)
      api.sendMessage(company.id, activeChatId, message, []).then((data) => {
        if (data.chats) {
          activeChatBoard.unshift(data.chats);
          setActiveChatBoard([...activeChatBoard]);
          setMessage("");
        }
      });
  };

  const _uploadFiles = () => {
    let promises = [];
    files.forEach((item) => {
      promises.push(
        new Promise((resolve, reject) => {
          api.uploadFiles(activeChatId, item).then((data) => {
            resolve(data);
          });
        })
      );
      Promise.all(promises).then((data) => {
        console.log(data);
      });
    });
  };

  const addFiles = (_files) => {
    for (let [, value] of Object.entries(_files)) {
      if (value.type.includes("image")) {
        files.push(value);
      }
    }
    setFiles([...files]);
  };

  return (
    <div className={"Chat"}>
      {!tabletChats && (
        <div className={"Chat_rooms"}>
          {chats.map(
            (item, key) =>
              item.message && (
                <ChatRoom
                  isActive={сhatId === key}
                  chatInfo={item}
                  key={key}
                  companyId={company.id}
                  openChat={showChat.bind(null, key)}
                />
              )
          )}
        </div>
      )}
      <div className={"Chat_splitter"} />
      {activeChatBoard && (
        <div className={"Chat_messenger"}>
          <div className={"Chat_messenger_messages"}>
            {activeChatBoard.map((item, key) => (
              <Messager item={item} key={key} userFrom={user && user.id !== item.from} />
            ))}
          </div>
          <div className={"Chat_messenger_send"}>
            <img
              className={"Chat_messenger_send_avatar"}
              src={"http://smartsvit.shn-host.ru" + (company && company.avatar && company.avatar)}
              alt={"avatar"}
            />
            <label>
              <img
                src={require("../../../public/img/files.png")}
                className={"Chat_messenger_send_files"}
                alt={"files"}
              />
              <input
                type="file"
                onChange={(e) => addFiles(e.target.files)}
                style={{ display: "none" }}
              />
            </label>
            <div className={"Chat_messenger_send_input"}>
              <input
                className={"Chat_messenger_send_input_input"}
                onChange={(e) => {
                  setMessage(e.target.value);
                }}
                value={message}
              />
              {files.map((item, key) => (
                <p key={key}>{item.name}</p>
              ))}
            </div>

            <img
              src={require("../../../public/img/send.png")}
              className={"Chat_messenger_send_button"}
              onClick={_sendMessage}
              alt={"send"}
            />
          </div>
        </div>
      )}
    </div>
  );
};
