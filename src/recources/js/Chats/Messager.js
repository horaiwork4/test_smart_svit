import React from "react";
import moment from "moment";

export const Messager = ({ item, userFrom }) => {
  return (
    <div
      className={
        userFrom
          ? "Chat_messenger_messages_message Chat_messenger_messages_message_to"
          : "Chat_messenger_messages_message Chat_messenger_messages_message_from"
      }
    >
      {/* <img
        className={"Chat_messenger_messages_message_avatar"}
        alt={"avatar"}
      /> */}
      <div>
        <div className={"Chat_messenger_messages_message_info"}>
          <p className={"Chat_messenger_messages_message_info_text"}>
            {item.body}
          </p>
          <div className={"files"}>
            {item.file && item.file.length > 0 &&
              item.file.map((item, key) => (
                <img
                  className={"files_image-file"}
                  alt={"файл"}
                  src={"http://smartsvit.shn-host.ru/chat/" + item.file}
                  key={key}
                />
              ))}
          </div>
        </div>
        <p className={"Chat_messenger_messages_message_info_time"}>
          {moment(item.created_at).format("hh:mm")}
        </p>
      </div>
    </div>
  );
};
