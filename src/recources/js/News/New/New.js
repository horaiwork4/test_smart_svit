import React, { useState } from "react";

export const New = ({ item }) => {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <div
      className={"New"}
      onClick={() => {
        setIsOpen(!isOpen);
      }}
    >
      <img
        src={require("../../../../public/img/smartNews.png")}
        className={"New_image"}
        alt={"smartNew"}
      />
      <div className={"New_info"}>
        <p className={"New_info_name"}>{item.name}</p>
        <p className={isOpen ? "New_info_desc New_info_desc_open" : "New_info_desc"}>
          {item.description}
        </p>
      </div>
    </div>
  );
};
