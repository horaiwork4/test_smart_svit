import React, { useEffect, useState } from "react";
import { getNews } from "../../../services/company/companyService";

import "./styles/News.scss";
import { New } from "./New/New";
import { getApi } from "../../../services/API";

export const News = ({ company }) => {
  const api = getApi().getCompany();
  const [news, setNews] = useState([]);

  useEffect(() => {
    let isSubscribed = true;
    if (company) {
      api.getNews(company.id).then((data) => {
        if (isSubscribed) {
          setNews(data.news);
        }
      });
    }
    return () => (isSubscribed = false);
  }, [company]);

  return (
    <div className={"News"}>
      {news.map((item, key) => (
        <New item={item} key={key} />
      ))}
    </div>
  );
};
