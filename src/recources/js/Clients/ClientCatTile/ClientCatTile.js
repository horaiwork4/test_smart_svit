import React, { useState } from "react";
import "./clientCatTile.scss";
import { Input } from "../../Components/Input/Input";

export function ClientCatTile({ count, title, onClickTile, onMessage, isEditable, isEdit, save }) {
  const [newName, setNewName] = useState("");

  return (
    <div className={"Tile"}>
      <div className={"Tile__quantitySection"}>
        <img
          className={"Tile__quantitySection-img"}
          alt="icon"
          src={require("../../../../public/img/group.png")}
        />
        <p className={"Tile__quantitySection-text"}>{count}</p>
      </div>
      <div className={"Tile__titleSection"}>
        {isEditable && isEdit ? (
          <Input defaultValue={title} autoFocus={true} value={newName} setValue={setNewName} />
        ) : (
          <p className={"Tile__title"}>{title}</p>
        )}
      </div>
      {isEditable ? (
        isEdit ? (
          <>
            <img
              className={"Tile__messageImg close"}
              alt="icon"
              src={require("../../../../public/img/rm_cat.png")}
              onClick={save.bind(null, null)}
            />
            <img
              className={"Tile__messageImg"}
              alt="icon"
              src={require("../../../../public/img/add.png")}
              onClick={save.bind(null, newName)}
            />
            <img
              className={"Tile__messageImg"}
              alt="icon"
              src={require("../../../../public/img/bin.png")}
              onClick={save.bind(null, null)}
            />
          </>
        ) : (
          <img
            className={"Tile__messageImg"}
            alt="icon"
            src={require("../../../../public/img/preferences.png")}
            onClick={onClickTile}
          />
        )
      ) : (
        <img
          className={"Tile__messageImg"}
          alt="icon"
          src={require("../../../../public/img/message.png")}
          onClick={onClickTile}
        />
      )}
    </div>
  );
}
