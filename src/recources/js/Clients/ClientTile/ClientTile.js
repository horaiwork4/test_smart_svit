import React from "react";
import "./clientTile.scss";

export function ClientTile({ avatar, name, handleIdk, handleMessage, handleDelete }) {
  return (
    <div className={"ClientTile"}>
      <img alt="avatar" className={"ClientTile__avatar"} src={avatar} />
      <div className={"ClientTile__nameSection"}>
        <p> {name} </p>
      </div>

      <div className={"ClientTile__actionsSection"}>
        <img
          alt="icon-action"
          className={"ClientTile__action idk"}
          src={require("../../../../public/img/idk.png")}
          onClick={handleIdk}
        />
        <img
          alt="icon-action"
          className={"ClientTile__action message"}
          src={require("../../../../public/img/chat.png")}
          onClick={handleMessage}
        />
        <img
          alt="icon-action"
          className={"ClientTile__action delete"}
          src={require("../../../../public/img/bin.png")}
          onClick={handleDelete}
        />
      </div>
    </div>
  );
}
