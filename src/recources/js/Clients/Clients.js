/* eslint-disable*/
import React, { useState, useEffect } from "react";
import { ActionButton } from "../../common/js/components/ActionButton";
import { ClientCatTile } from "./ClientCatTile/ClientCatTile";
import { ClientTile } from "./ClientTile/ClientTile";
import "./styles.scss";
import { getApi } from "../../../services/API";
import { Loader } from "../../common/js/components/Loader";
import { Modal } from "../../common/js/components/Modal";
import { Input } from "../Components/Input/Input";

export const Clients = ({ company }) => {
  const apiClients = getApi().getClients();

  const [clientsCategory, setClientsCategory] = useState([]);
  const [category, setCategory] = useState(null);
  const [clientsInCategory, setClientsInCategory] = useState([]);
  const [titleCategory, setTitleCategory] = useState("");

  const [phone, setPhone] = useState("");
  const [phoneError, setPhoneError] = useState(false);

  const [userInSystem, setUserInSystem] = useState(false);

  const [addClientModalVisible, setAddClientModalVisible] = useState(false);

  const [catName, setCatName] = useState("");
  const [catError, setcatError] = useState(false);
  const [newCatVisible, setNewCatVisible] = useState(false);

  const [isEditable, setEditable] = useState(false);

  const [isLoading, setLoading] = useState(false);

  const validateInvitePhone = (ph) => {
    const regexPhone = new RegExp(/^[380]\d{11}$/g);

    return regexPhone.test(ph);
  };

  useEffect(() => {
    if (company && company.id) {
      setLoading(true);
      apiClients.getClientCategories(company.id).then((data) => {
        setClientsCategory(data.client_category);
        setLoading(false);
      });
    }
  }, [company]);

  useEffect(() => {
    if (company && category) {
      setLoading(true);
      apiClients.getClientsInCategory(company.id, category, 1).then((data) => {
        setClientsInCategory(data.data);
        setLoading(false);
      });
    }
  }, [category, company]);

  const handleCatTile = (item) => {
    if (isEditable) {
      item.isEdit = !item.isEdit;
      setClientsCategory([...clientsCategory]);
    } else {
      setTitleCategory(item.name);
      setCategory(item.id);
    }
  };

  const changeCat = (id, itemKey, name) => {
    if (name === null) {
      clientsCategory[itemKey].isEdit = false;
      setClientsCategory([...clientsCategory]);
    }
  };

  const catTile =
    clientsCategory &&
    clientsCategory.length > 0 &&
    clientsCategory.map((item, key) => {
      return (
        <ClientCatTile
          isEditable={isEditable}
          isEdit={item.isEdit ? item.isEdit : false}
          save={changeCat.bind(null, item.id, key)}
          key={item.id}
          count={item.count}
          title={item.name}
          onClickTile={handleCatTile.bind(null, item)}
          onMessage={() => console.log("creating message to group")}
          active={item.active}
        />
      );
    });

  const clientTile =
    clientsInCategory.length > 0 &&
    clientsInCategory.map((item) => {
      const user_name = `${item.client.user.first_name} ${item.client.user.last_name}`;
      return (
        <ClientTile
          key={item.client.id}
          avatar={"http://smartsvit.shn-host.ru" + item.client.user.avatar}
          name={user_name}
          handleIdk={() => console.log("idk method")}
          handleMessage={() => console.log("Redirect to chat")}
          handleDelete={() => console.log("Delete client")}
        />
      );
    });

  const clearField = () => {
    setPhone("");
    setPhoneError(false);
  };

  const handleAddClientModal = () => {
    setAddClientModalVisible(!addClientModalVisible);
    clearField();
  };

  const inviteNewUser = () => {
    if (!validateInvitePhone(phone)) {
      console.log("false");
      setPhoneError(true);
    }

    if (phone && !!phoneError) {
      apiClients.clientSearch(phone).then((data) => {
        if (data && data.length > 0) {
          setUserInSystem(true);
        }
      });
    }

    if (company && company.id && phone && phoneError === false && userInSystem === false) {
      setLoading(true);
      apiClients.companyInviteUser(company.id, phone).then((data) => {
        setLoading(false);
        handleAddClientModal();
      });
    }
  };

  const addClientModalChildren = (
    <Input
      name={"Введите номер "}
      multiLine={false}
      value={phone}
      error={phoneError || userInSystem}
      setValue={setPhone}
      withEndLine={true}
      type={"numeric"}
      helperText={
        userInSystem
          ? "Пользователь зарегистрирован в приложении"
          : "Введите номер в формате 380990200416"
      }
    />
  );

  const addClientCategory = (
    <Input
      name={"Введите название"}
      multiLine={false}
      value={catName}
      error={catError}
      setValue={setCatName}
      withEndLine={true}
      type={"text"}
    />
  );

  return (
    <div className={"Clients"}>
      <div className={"Clients__topbar"}>
        <ActionButton
          title="Добавить клиента"
          onClick={handleAddClientModal}
          icon={require("../../../public/img/add.png")}
        />
        <ActionButton
          title="Настроить"
          onClick={() => {
            setEditable(!isEditable);
          }}
          icon={require("../../../public/img/preferences.png")}
        />
        <ActionButton
          title="Создать категорию"
          onClick={() => {
            setNewCatVisible(true);
          }}
          icon={require("../../../public/img/add.png")}
        />
      </div>
      <div className={"Clients__content"}>
        <div className={"Clients__content-catTiles"}>{catTile}</div>
        <div className={"Clients__content-clientsTiles"}>
          <p className={"Clients__content-clientsTiles-title"}>
            {!!titleCategory ? titleCategory : null}
          </p>
          {clientTile ? clientTile : "В этой категории нет клиентов"}
        </div>
      </div>
      {isLoading && <Loader />}

      <Modal
        acceptLabel="Подтвердить"
        closeLabel="Отменить"
        handleClose={handleAddClientModal}
        onAccept={inviteNewUser}
        show={addClientModalVisible}
        children={addClientModalChildren}
        title="Добавить клиента"
      />

      <Modal
        acceptLabel="Подтвердить"
        closeLabel="Отменить"
        handleClose={() => {
          setCatName("");
          setNewCatVisible(false);
        }}
        onAccept={() => {
          apiClients.createNewClientsCategory(company.id, catName).then((data) => {
            if (data.category) {
              data.category.count = 0;
              clientsCategory.push(data.category);
              setClientsCategory([...clientsCategory]);
            }
            setCatName("");
            setNewCatVisible(false);
          });
        }}
        show={newCatVisible}
        children={addClientCategory}
        title="Добавить категорию"
      />
    </div>
  );
};
