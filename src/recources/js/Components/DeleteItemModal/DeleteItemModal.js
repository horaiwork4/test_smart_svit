import React from "react";
import { Modal } from "../../../common/js/components/Modal";

export function DeleteItemModal({ hideModal, visibleModal, onAccept }) {
  return (
    <Modal
      handleClose={hideModal}
      show={visibleModal}
      onAccept={onAccept}
      title={"Вы уверены что хотите удалить акцию?"}
      closeLabel={"Отменить"}
      acceptLabel={"Удалить"}
    />
  );
}
