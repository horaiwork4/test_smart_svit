import React from "react";

import "./style.scss";

export const ImageLoader = ({
  className,
  onClick,
  photo,
  alt,
  isLink = false,
}) => {
  return (
    <div onClick={onClick ? onClick : null}>
      {photo ? (
        isLink ? (
          <img
            className={"imagePicker mr30"}
            src={"http://smartsvit.shn-host.ru/" + photo}
            alt={alt}
          />
        ) : (
          <img className={"imagePicker mr30"} src={photo} alt={alt} />
        )
      ) : (
        <div className={"imagePicker " + className}>
          <img
            src={require("../../../../public/img/greyPlus.png")}
            className={"imagePicker_plus"}
            alt={""}
          />
        </div>
      )}
    </div>
  );
};
