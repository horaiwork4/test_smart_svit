import React from "react";

import "./Input.scss";

export const Input = ({
  name,
  defaultValue,
  setValue,
  value,
  withEndLine,
  type,
  multiLine,
  error,
  helperText,
  autoFocus = false,
}) => {
  let classname = error ? "Input_input errorInput" : "Input_input";
  classname = withEndLine ? classname + " downLine" : classname;

  return (
    <div className={"Input"}>
      <p className={"Input_name"}>{name}</p>
      {multiLine ? (
        <>
          <textarea
            placeholder={defaultValue}
            className={classname}
            onChange={(e) => setValue(e.target.value)}
          />
          {helperText && (
            <p
              className={
                error
                  ? "Input_helperText Input_helperText_error"
                  : "Input_helperText"
              }
            >
              {helperText}
            </p>
          )}
        </>
      ) : (
        <>
          <input
            className={classname}
            value={value}
            autoFocus={autoFocus}
            type={type}
            placeholder={defaultValue}
            onChange={(e) => {
              setValue(e.target.value);
            }}
          />{" "}
          {helperText && (
            <p
              className={
                error
                  ? "Input_helperText Input_helperText_error"
                  : "Input_helperText"
              }
            >
              {helperText}
            </p>
          )}
        </>
      )}
    </div>
  );
};
