import React from "react";
import "./style.scss";
import ImageGallery from "react-image-gallery";

export const CardItem = ({ items, isSale, isPromo, handleDeleteItem }) => {
  return items.map((item) => {
    const imagesLink = [];

    if (isSale) {
      const links = item.promotion.image.split(",");

      links.map((item) => {
        const original = "http://smartsvit.shn-host.ru" + item;

        imagesLink.push({ original });
      });
    }
    console.log(imagesLink);
    return (
      <div className="Sale" key={isSale ? item.promotion.id : item.board_id}>
        {isSale && imagesLink.length > 0 ? (
          <ImageGallery
            items={imagesLink}
            showThumbnails={false}
            showPlayButton={false}
            sizes={{ height: "10rem" }}
          />
        ) : null}
        {isPromo && item && item.board && item.board.image.length > 0 ? (
          <div className={"Promo___imageblock"}>
            <ImageGallery
              items={item.board.image.map((image) => {
                image.original = "http://smartsvit.shn-host.ru/" + image.avatar;
                return image;
              })}
              showThumbnails={false}
              showPlayButton={false}
              sizes={{ height: "14rem" }}
            />
            <div className={"Promo__blurblock"}>
              <p className={"Promo__blurblock-vacancy"}>
                {!!item && !!item.board && !!item.board.type !== undefined
                  ? +item.board.type === 1
                    ? "Недвижимость"
                    : +item.board.type === 2
                    ? "Информационный"
                    : +item.board.type === 3
                    ? "Вакансии"
                    : null
                  : null}
              </p>
              <p className={"Promo__blurblock-status"}>
                {!!item && !!item.board && !!item.board.status_id
                  ? item.board.status_id === 1
                    ? "Черновик"
                    : item.board.status_id === 2
                    ? "На модерации"
                    : item.board.status_id === 3
                    ? "Доработка"
                    : item.board.status_id === 4
                    ? "Опубликовано"
                    : null
                  : null}
              </p>
            </div>
          </div>
        ) : null}
        <div className={"Sale__action"}>
          <button
            className={"Sale__action-btn"}
            onClick={handleDeleteItem && handleDeleteItem.bind(null, item)}
          >
            <img
              src={require("../../../../public/img/trash.png")}
              className={"Sale__action-img"}
              alt={"trash"}
            />
          </button>
        </div>

        <p className={"Sale__name"}> {isSale ? item.promotion.name : item.board.name} </p>
        {isSale ? (
          <div className={"Sale__date"}>
            <img
              src={require("../../../../public/img/sale_date.png")}
              className={"Sale__date-img"}
              alt={"date"}
            />
            <p className={"Sale__date-textdate"}>
              {item.promotion.startDateTime.substring(8, 10)}.
              {item.promotion.startDateTime.substring(5, 7)} -{" "}
              {item.promotion.endDateTime.substring(8, 10)}.
              {item.promotion.endDateTime.substring(5, 7)}
            </p>
          </div>
        ) : null}
      </div>
    );
  });
};
