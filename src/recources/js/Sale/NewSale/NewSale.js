/* eslint-disable no-unused-vars */
import React, { useState, useEffect, useRef } from "react";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../../store/actions/PrewPage";
import { setHeaderSaveBtn } from "../../../../store/actions/actions";
import { ImageLoader } from "../../Components/ImageLoader/ImageLoader";
import { Input } from "../../Components/Input/Input";
import "./styles/style.scss";
import "react-date-range/dist/styles.css";
import "react-date-range/dist/theme/default.css";
import { DateRange } from "react-date-range";
import { Modal } from "../../../common/js/components/Modal";
import { ru } from "date-fns/locale";
import { useHistory } from "react-router-dom";
import { Loader } from "../../../common/js/components/Loader";
import { getApi } from "../../../../services/API";
import imageCompression from "browser-image-compression";

export const NewSale = ({ company }) => {
  const foodApi = getApi().getFood();
  const salesApi = getApi().getSales();
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(false);

  const [desc, setDesc] = useState("");
  const [descError, setDescError] = useState(false);

  const [nameLink, setNameLink] = useState("");
  const [nameLinkError, setNameLinkError] = useState("");

  const [link, setLink] = useState("");
  const [linkError, setLinkError] = useState("");

  const [nameCoupon, setNameCoupon] = useState("");
  const [nameCouponError, setNameCouponError] = useState("");

  const [descCoupon, setDescCoupon] = useState("");
  const [descCouponError, setDescCouponnError] = useState("");

  const [discont, setDiscont] = useState("");
  const [discontError, setDiscontError] = useState("");

  const [startDateSale, setStartDateSale] = useState("");
  const [endDateSale, setEndDateSale] = useState("");

  const [dateSaleError, setDateSaleError] = useState(false);

  const [photos, setPhotos] = useState([]);
  const [rawPhotos, setRawPhotos] = useState([]);
  const [photosError, setPhotosError] = useState([]);

  const [compID, setCompID] = useState(null);

  const [showPicker, setShowPicker] = useState(false);

  const [checkedAddBtn, setCheckedAddBtn] = useState(false);
  const [checkedCoupBtn, setCheckedCoupBtn] = useState(false);

  const [isLoading, setLoading] = useState(false);

  const colors = ["#9900FF", "#339933", "#FF6666", "#DE0DD6", "#FFA941"];
  const [pickdeId, setPickedId] = useState(0);

  const dispatch = useDispatch();
  const history = useHistory();
  const fileRef = useRef();
  const [ranges, setRanges] = useState([
    {
      startDate: new Date(),
      endDate: new Date(),
      key: "selection",
    },
  ]);

  useEffect(() => {
    dispatch(changeIsActivePage(true));
    dispatch(
      setHeaderSaveBtn(
        createSale.bind(
          null,
          compID,
          name,
          desc,
          startDateSale,
          endDateSale,
          rawPhotos,
          nameLink,
          link,
          nameCoupon,
          descCoupon,
          discont
        )
      )
    );
  }, [dispatch]);

  useEffect(() => {
    if (!!company && !!company.id) {
      setCompID(company.id);
    }
  }, [company]);

  useEffect(() => {
    if (!!compID) {
      dispatch(
        setHeaderSaveBtn(
          createSale.bind(
            null,
            compID,
            name,
            desc,
            startDateSale,
            endDateSale,
            rawPhotos,
            nameLink,
            link,
            nameCoupon,
            descCoupon,
            discont
          )
        )
      );
    }
  }, [
    compID,
    name,
    desc,
    startDateSale,
    endDateSale,
    rawPhotos,
    nameLink,
    link,
    nameCoupon,
    descCoupon,
    discont,
  ]);

  const handleShowPicker = () => {
    setShowPicker(!showPicker);
  };

  const formatDate = (date) => {
    let dd = date.getDate();
    if (dd < 10) {
      dd = "0" + dd;
    }

    let mm = date.getMonth() + 1;
    if (mm < 10) {
      mm = "0" + mm;
    }

    let yy = date.getFullYear();
    // if (yy < 10) {
    //   yy = "20" + yy;
    // }

    // return dd + "-" + mm + "-" + yy;
    return yy + "-" + mm + "-" + dd + " 00:00:00";
  };

  const handleSelect = () => {
    let firstDateOfSale = ranges[0].startDate;
    let endDateOfSale = ranges[0].endDate;

    setStartDateSale(formatDate(firstDateOfSale));
    setEndDateSale(formatDate(endDateOfSale));
    handleShowPicker();
  };

  const pickerRangeDate = (
    <DateRange
      editableDateInputs={true}
      onChange={(item) => setRanges([item.selection])}
      moveRangeOnFirstSelection={false}
      ranges={ranges}
      locale={ru}
    />
  );

  const uploadPhotos = (image) => {
    return new Promise((resolve, reject) => {
      foodApi.uploadPhoto(image).then((data) => {
        resolve(data.data.url);
      });
    });
  };

  const addPhoto = (_photos) => {
    if (_photos.length > 0) {
      rawPhotos.push(_photos[_photos.length - 1]);
      setRawPhotos([...rawPhotos]);

      let path = URL.createObjectURL(_photos[0]);
      photos.push(path);
      setPhotos([...photos]);
    }
  };

  // useEffect(() => {
  //   if (name.length > 9) setNameError(false);
  //   else setNameError(true);
  // }, [name]);

  // useEffect(() => {
  //   if (desc.length > 19) setDescError(false);
  //   else setDescError(true);
  // }, [desc]);

  const createSale = (
    compID,
    name,
    desc,
    startDateSale,
    endDateSale,
    images,
    nameLink,
    link,
    nameCoupon,
    descCoupon,
    discont
  ) => {
    let error = false;

    if (name.length < 9) {
      error = true;
      setNameError(true);
    }

    if (desc.length < 19) {
      error = true;
      setDescError(true);
    }

    if (images.length === 0) {
      error = true;
      setPhotosError(true);
    }

    if (!startDateSale || !endDateSale) {
      error = true;
      setDateSaleError(true);
    }

    let photos = [];
    if (images.length) {
      images.forEach((image) => {
        photos.push(uploadPhotos(image));
      });
    }

    if (!error) {
      setLoading(true);
      Promise.all(photos).then((images) => {
        console.log(images);
        salesApi
          .newPromotion(
            compID,
            name,
            desc,
            startDateSale,
            endDateSale,
            images,
            nameLink,
            link,
            nameCoupon,
            descCoupon,
            discont
          )
          .then((data) => {
            if (data && data.status === "success") {
              setLoading(false);
              history.goBack();
            } else {
              console.log(data);
              setLoading(false);
            }
          });
      });
    } else {
      alert("Fill all fields please");
    }
  };

  return (
    <div className={"NewSale"}>
      <div className={"NewSale__generalInfo"}>
        <p className={"NewSale__duration"}>Длительность акции</p>
        <button
          className={"NewSale__button"}
          onClick={handleShowPicker}
          style={{ color: !!dateSaleError ? "red" : "black" }}
        >
          {!!startDateSale && !!endDateSale
            ? `${startDateSale.slice(5, 7)}.${startDateSale.slice(8, 11)} -  ${endDateSale.slice(
                5,
                7
              )}.${endDateSale.slice(8, 11)}`
            : "Выбрать даты"}
        </button>
        <Modal
          show={showPicker}
          title={"Выбрать даты"}
          onAccept={handleSelect}
          handleClose={handleShowPicker}
          children={pickerRangeDate}
          isRangePicker={true}
        />
        <div className={"NewSale__photos"}>
          {photos.map((item, key) => (
            <ImageLoader photo={item} key={key} alt={"photo"} />
          ))}
          <label>
            <ImageLoader error={photosError} />
            <input
              type="file"
              id={"files"}
              ref={fileRef}
              accept=".jpg, .jpeg, .png"
              onChange={(e) => addPhoto(e.currentTarget.files)}
              style={{ display: "none" }}
            />
          </label>
        </div>
        <div className={"NewSale__namesale"}>
          <Input
            name={"Название акции"}
            defaultValue={"Название акции *"}
            value={name}
            helperText={`Минимум символов ${name.length}/10`}
            setValue={setName}
            error={nameError}
            withEndLine={true}
            type={"text"}
          />
        </div>
        <div className={"NewSale__descsale"}>
          <Input
            name={"Описание"}
            defaultValue={"Описание *"}
            multiLine={true}
            helperText={`Минимум символов ${desc.length}/20`}
            value={desc}
            error={descError}
            setValue={setDesc}
            withEndLine={true}
            type={"text"}
          />
        </div>
      </div>

      {isLoading && <Loader />}

      <div className={"NewSale__additionalInfoBtn"}>
        <input
          className={"NewSale__additionalInfoBtn-btn"}
          type="checkbox"
          checked={checkedAddBtn}
          onChange={() => {
            setCheckedAddBtn(!checkedAddBtn);
            console.log("checkAddBtn");
          }}
        />
        <label className={"NewSale__additionalInfoBtn-btntext"}>
          Использовать дополнительную кнопку
        </label>

        {checkedAddBtn ? (
          <div className={"NewSale__additionalInfoBtn-link"}>
            <Input
              name={"Название ссылки"}
              defaultValue={"Строительство - Домофон"}
              value={nameLink}
              setValue={setNameLink}
              error={nameLinkError}
              withEndLine={true}
              type={"text"}
            />
          </div>
        ) : null}
        {checkedAddBtn ? (
          <Input
            name={"Укажите URL"}
            defaultValue={"https://www.google.com/"}
            value={link}
            error={linkError}
            setValue={setLink}
            withEndLine={true}
            type={"text"}
          />
        ) : null}
      </div>

      <div className={"NewSale__Coupon"}>
        <input
          className={"NewSale__Coupon-btn"}
          type="checkbox"
          checked={checkedCoupBtn}
          onChange={() => {
            setCheckedCoupBtn(!checkedCoupBtn);
            console.log("checkCoupBtn");
          }}
        />
        <label className={"NewSale__Coupon-btntext"}>Использовать купон</label>
        {!!checkedCoupBtn ? (
          <div className={"NewSale__Coupon-name"}>
            <Input
              name={"Название купона"}
              defaultValue={"Строительство - Домофон"}
              value={nameCoupon}
              setValue={setNameCoupon}
              error={nameCouponError}
              withEndLine={true}
              type={"text"}
            />
          </div>
        ) : null}
        {!!checkedCoupBtn ? (
          <Input
            name={"Описание купона"}
            defaultValue={"Тайский, Косметический, Криомассаж"}
            value={descCoupon}
            error={descCouponError}
            setValue={setDescCoupon}
            withEndLine={true}
            type={"text"}
          />
        ) : null}
        {!!checkedCoupBtn ? (
          <Input
            name={"Укажите размер скидки (Например -20%)"}
            defaultValue={"-20%"}
            value={discont}
            error={discontError}
            setValue={setDiscont}
            withEndLine={true}
            type={"text"}
          />
        ) : null}

        {!!checkedCoupBtn ? (
          <p className={"NewSale__Coupon-colortext"}>Выберите цвет купона</p>
        ) : null}

        {!!checkedCoupBtn &&
          colors.map((color, id) => (
            <button
              onClick={() => setPickedId(id)}
              className={"NewSale__Coupon-colorbtn"}
              style={{ background: color, opacity: pickdeId === id ? 1 : 0.25 }}
              key={id}
            ></button>
          ))}
      </div>
    </div>
  );
};
