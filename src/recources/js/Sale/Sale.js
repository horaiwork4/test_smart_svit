import React, { useEffect } from "react";
import { SaleBlock } from "./SaleBlock";
import { NewSale } from "./NewSale/NewSale";
import { useDispatch } from "react-redux";
import { changeIsActivePage } from "../../../store/actions/PrewPage";
import { Switch, Route, useHistory } from "react-router-dom";

export const Sale = ({ company }) => {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    history.listen((location, action) => {
      if (location.pathname === "/sale") dispatch(changeIsActivePage(false));
    });
  }, [history, dispatch]);

  return (
    <Switch>
      <Route exact path={"/sale"}>
        <SaleBlock company={company} />
      </Route>
      <Route exact path={"/sale/newSale"}>
        <NewSale company={company} />
      </Route>
    </Switch>
  );
};
