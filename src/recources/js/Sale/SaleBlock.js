import React, { useState, useEffect } from "react";
import "./styles/Ads.scss";
import { CardItem } from "../Components/CardItem/CardItem";
import { Link } from "react-router-dom";
import { getApi } from "../../../services/API";
import { Loader } from "../../common/js/components/Loader";

export const SaleBlock = ({ company }) => {
  const api = getApi().getSales();
  const [sales, setSales] = useState(null);

  const [isLoading, setLoading] = useState(false);

  useEffect(() => {
    if (company && company.id) {
      setLoading(true);
      api.getSales(company.id, 1).then((data) => {
        setSales(data.promotion.data);
        setLoading(false);
      });
    }
  }, []);

  // console.log(sales);

  const handleDeleteSale = (item) => {
    setLoading(true);
    api.deleteProm(company.id, item.promotion.id).then((data) => {
      if (data.status === "success") {
        setSales(sales.filter((i) => i.promotion.id !== item.promotion.id));
        setLoading(false);
      }
    });
  };

  return (
    <>
      <div className={"SalesAddBlock"}>
        <img
          className={"SalesAddBlock__img"}
          alt={"add"}
          src={require("../../../public/img/add.png")}
        />
        <Link to={"/sale/newSale"}>
          <button className={"SalesAddBlock__text"}>Создать акцию</button>
        </Link>
      </div>
      {isLoading && <Loader />}
      {!!sales && sales.length > 0 ? (
        <div className={"SalesBlock"}>
          <CardItem items={sales} isSale={true} handleDeleteItem={handleDeleteSale} />
        </div>
      ) : (
        !isLoading && <div className={"SalesBlock__exceptsales"}>Акций нет</div>
      )}
    </>
  );
};
