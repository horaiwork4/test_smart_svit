import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSpinner } from "@fortawesome/free-solid-svg-icons";
import { useHistory } from "react-router-dom";
import { useSelector } from "react-redux";

import { AUTH } from "../../../localization/auth";

import "./styles/PickCompany.scss";
import { getApi } from "../../../services/API";

export const PickCompany = ({ token }) => {
  const api = getApi().getCompany();
  const [loading, setLoading] = useState(false);
  const [companys, setCompanys] = useState([]);

  const history = useHistory();
  const language = useSelector((state) => state.language.language);

  const pick = (company_id) => {
    localStorage.setItem("company", JSON.stringify(companys[company_id]));
    history.push("/");
  };

  useEffect(() => {
    setLoading(true);
    api.getComapny(token).then((data) => {
      if (data !== "error") setCompanys(data);
      setLoading(false);
    });
  }, [token]);

  return (
    <>
      <p className={"chooseCompany"}>{AUTH.CHOOSE_COMPANY[language]}</p>
      <div className={"Company"}>
        {loading && (
          <FontAwesomeIcon
            icon={faSpinner}
            className={"Button_loopLoading"}
            size="lg"
            color={"#649400"}
          />
        )}
        {companys.map((item, id) => (
          <div
            className={"Company__item"}
            onClick={pick.bind(null, id)}
            key={id}
          >
            <div
              style={{ display: "flex", alignItems: "center", width: "80%" }}
            >
              {!!item.avatar ? (
                <img
                  className={"Company__item_avatar"}
                  src={"http://smartsvit.shn-host.ru" + item.avatar}
                  alt={"company logo"}
                />
              ) : (
                <div className={"Company__item_avatar_name_container"}>
                  <span className={"Company__item_avatar_name"}>
                    {item.name[0]}
                  </span>
                </div>
              )}
              <p className={"Company__item_name"} key={id}>
                {item.name}
              </p>
            </div>
            <img
              className={"Company__item_arrow"}
              src={require("./../../../public/img/rigrhArrow.png")}
              alt={"arrow"}
            />
          </div>
        ))}
      </div>
    </>
  );
};
