import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";

import "./styles/Auth.scss";
import { AUTH } from "../../../localization/auth";

import { Input } from "../../common/js/components/Input";
import { Button } from "../../common/js/components/Button";
import { changeLanguage } from "../../../store/actions/localization";
import { PickCompany } from "../Pick_company/PickCompany";
import { getApi } from "../../../services/API";

export const Auth = () => {
  const api = getApi().getAuth();
  const language = useSelector((state) => state.language.language);
  const languages = useSelector((state) => state.language.languages);
  const [logged, setLogged] = useState(false);
  const [loading, setLoading] = useState(false);
  const [formError, setFormError] = useState(false);
  const [token, setToken] = useState(null);
  const [skipLogin, setSkipLogin] = useState(false);

  const [value, setValue] = useState(0);

  const history = useHistory();

  const [formControls, setFormControls] = useState({
    phone: {
      value: "",
      type: "tel",
      srcIcon: require("../../../public/img/auth_input1.png"),
      placeholder: "+380991234567",
    },
    password: {
      value: "",
      type: "password",
      srcIcon: require("../../../public/img/auth_input2.png"),
      placeholder: "**********",
    },
  });

  const dispatch = useDispatch();

  useEffect(() => {
    const language = localStorage.getItem("lang");
    const tempToken = localStorage.getItem("token");

    if (!!language) {
      languages.forEach((el, index) => {
        if (el.value === language) {
          [languages[0], languages[index]] = [languages[index], languages[0]];
        }
      });
    }

    if (tempToken) {
      setToken(tempToken);
      setSkipLogin(true);
    }

    if (localStorage.getItem("company") && localStorage.getItem("token")) {
      history.push("/");
    }

    dispatch(changeLanguage(languages, !!language ? language : "RU"));
    // eslint-disable-next-line
  }, []);

  const handleSelectLang = (e) => {
    setValue(e.target.value);

    dispatch(changeLanguage(languages, languages[e.target.value].value));
  };

  function onInputChange(e, el) {
    const field = formControls[el];

    field.value = e.target.value;

    setFormControls({ ...formControls });
  }

  function onSubmit(e) {
    e.preventDefault();
    setLoading(true);
    api
      .authService(formControls.phone.value, formControls.password.value)
      .then((data) => {
        if (data.success != null) {
          setLogged(true);
          setToken(data.success);
          localStorage.setItem("token", data.success);
        } else setFormError(true);
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  }

  let options = languages.map((item, index) => {
    return (
      <option key={index} value={index}>
        {item.value}
      </option>
    );
  });

  const fields = Object.keys(formControls).map((el, index) => {
    return (
      <Input
        key={index}
        className={formError ? "Auth__form_field error" : "Auth__form_field"}
        {...formControls[el]}
        label={
          index === 0 ? AUTH.PHONE_LABEL[language] : AUTH.PASS_LABEL[language]
        }
        onChange={(e) => {
          setFormError(false);
          onInputChange(e, el);
        }}
      />
    );
  });

  return (
    <div className={"Auth"}>
      <div className={"Auth__info"}>
        <select
          className={"Auth__info-btnlang"}
          value={value}
          onChange={handleSelectLang}
        >
          {options}
        </select>
        <div className="Auth__info-logo-container">
          <picture>
            <source
              srcSet={require("../../../public/img/auth_logo_sm.png")}
              media={"max-width: 560px"}
            />
            <source
              srcSet={require("../../../public/img/auth_logo_rg.png")}
              media={"max-width: 992px"}
            />
            <source
              srcSet={require("../../../public/img/auth_logo_lg.png")}
              media={"min-width: 993px"}
            />

            <img
              src={require("../../../public/img/auth_logo_xl.png")}
              className={"Auth__info-logo"}
              alt={"Smart Svit Logo"}
            />
          </picture>

          <span className="Auth__info-name">
            Smart <br /> Svit
          </span>
        </div>

        <p className="Auth__info-description">
          {AUTH.DESCRIPTION_FIRST[language]}
          <br />

          {AUTH.DESCRIPTION_SECOND[language]}
        </p>
      </div>

      <div
        className={
          logged
            ? "Auth__form-animated"
            : skipLogin
            ? "Auth__form Auth_form_mobile_logged"
            : "Auth__form"
        }
      >
        {!skipLogin && (
          <div className={"Auth__form-ellement"}>
            <form className={"Auth__form-form"} onSubmit={(e) => onSubmit(e)}>
              <p className={"Auth__form-headliner"}>
                {AUTH.FORM_HEADLINER[language]}
              </p>

              {fields}

              <p className={"Auth__form-forgot-password"}>
                {AUTH.FORGOT[language]}
              </p>

              <Button
                className="Auth__form_button"
                type={"submit"}
                loading={loading}
                label={AUTH.BUTTON[language]}
                disabled={
                  formControls.phone.value.length < 1 ||
                  formControls.password.value.length < 1
                }
              />
            </form>
          </div>
        )}

        <div className={"Auth__form-ellement"}>
          <PickCompany token={token} />
        </div>
      </div>
    </div>
  );
};
